import React, { useState, useEffect } from "react";
import axios from "axios";
import { AsyncStorage, Alert } from "react-native";

import { StyleSheet, View, ScrollView } from "react-native";
import Input from "../components/Input";
import BigButton from "../components/Button";
import PageWrapper from "../components/PageWrapper";

import { API_URL } from "../utils/API_URL";

const styles = StyleSheet.create({
  inputsWrapper: {
    marginTop: 10
  },
  buttonWrapper: {
    marginTop: 20
  }
});

export default function LoginScreen({ navigation }) {
  const [userNameValue, setUserNameValue] = useState("");
  const requiredFields = navigation.getParam("fields", []);

  let fieldArray = [];

  useEffect(() => {
    console.log("requiredFields");
    console.log(requiredFields);
  }, []);

  const createFieldArray = fields => {
    fields.map(field => fieldArray.push(""));
  };

  const checkEmailAdress = value => {
    console.log("Email", value);
    if (!value) return;
    if (!value.match(/.+\@.+\..+/)) {
      Alert.alert("Incorrect email adress!");
    }
  };

  const createFieldsResponse = () => {
    let fieldsArray = [];
    requiredFields.map((field, index) =>
      fieldsArray.push({
        type: field.type,
        name: field.name,
        value: fieldArray[index]
      })
    );
    return fieldsArray;
  };

  const setValue = (value, index, number) => {
    if (number) value = Number(value);
    fieldArray[index] = value;
  };

  retrieveToken = async () => {
    try {
      const value = await AsyncStorage.getItem("token");
      if (value !== null) {
        return value;
      }
    } catch (error) {
      console.log(error);
    }
  };

  async function handleLoginToQuiz() {
    const token = await retrieveToken();
    console.log(fieldArray);
    console.log(createFieldsResponse());
    const fieldsRes = createFieldsResponse();

    const data = {
      name: userNameValue,
      fields: fieldsRes
    };
    const headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + token
    };
    axios
      .post(API_URL + "/api/auth/user/activate", data, { headers })
      .then(res => {
        if (res.status === 201) {
          navigation.navigate("quizStart", { userNameValue });
        } else {
          console.log("ERROR");
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  return (
    <PageWrapper>
      <View style={{ flex: 1, marginTop: 10 }}>
        <View style={styles.inputsWrapper}>
          <Input
            placeholder="Enter game nick..."
            onChangeText={text => setUserNameValue(text)}
            value={userNameValue}
          />
        </View>
        {requiredFields.map((field, index) => (
          <View key={index} style={styles.inputsWrapper}>
            {field.type === "email" && (
              <Input
                keyboardType="email-address"
                placeholder={"Enter " + field.name + " ..."}
                onChangeText={text => setValue(text, index)}
                onBlur={() => checkEmailAdress(fieldArray[index])}
                value={fieldArray[index]}
              />
            )}
            {field.type === "number" && (
              <Input
                keyboardType="numeric"
                placeholder={"Enter " + field.name + " ..."}
                onChangeText={text => setValue(text, index, true)}
                value={fieldArray[index]}
              />
            )}
            {field.type === "text" && (
              <Input
                placeholder={"Enter " + field.name + " ..."}
                onChangeText={text => setValue(text, index)}
                value={fieldArray[index]}
              />
            )}
          </View>
        ))}
        <View style={styles.buttonWrapper}>
          <BigButton text="Next" onPress={() => handleLoginToQuiz()} />
        </View>
      </View>
    </PageWrapper>
  );
}
