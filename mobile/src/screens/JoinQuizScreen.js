import React, { useState } from "react";

import { Text, StyleSheet, View, Image } from "react-native";
import Input from "../components/Input";
import BigButton from "../components/Button";
import Row from "../components/Row";
import PageWrapper from "../components/PageWrapper";

import handleGoToQuiz from "../utils/handleGoToQuiz";

const styles = StyleSheet.create({
  appNameStyle: {
    fontSize: 30,
    color: "#112233",
    fontWeight: "bold",
    marginTop: 30
  },
  smallTextStyle: {
    fontSize: 16,
    color: "#112233",
    fontWeight: "bold",
    marginTop: 20
  },
  infoText: {
    color: "#112233",
    fontSize: 20,
    padding: 20,
    fontWeight: "bold",
    textAlign: "center"
  },
  errorText: {
    color: "red"
  },
  screenView: {
    padding: 10,
    flex: 1,
    marginTop: 40
  },
  inputsWrapper: {
    marginTop: 20
  }
});

export default function JoinQuizScreen({ navigation }) {
  const [userNameValue, setUserNameValue] = useState("");

  return (
    <PageWrapper>
      <View style={styles.screenView}>
        <Row justify="center">
          <Image
            style={{ width: 170, height: 50 }}
            source={require("../../assets/logo.png")}
          />
        </Row>
        <View style={styles.inputsWrapper}>
          <Input
            placeholder="Enter game code"
            onChangeText={text => setUserNameValue(text)}
            value={userNameValue}
          />
          <BigButton
            text="Go to quiz"
            onPress={() => handleGoToQuiz(userNameValue, navigation)}
          />
        </View>
        <Row justify="center">
          <Text style={styles.smallTextStyle}>OR</Text>
        </Row>
        <View style={styles.inputsWrapper}>
          <BigButton
            text="Scan QR Code"
            onPress={() => navigation.navigate("scanner")}
            reverse
          />
        </View>
        <Row justify="center">
          <Text style={styles.infoText}>
            Welcome to our gorgeus app! You can get into quiz by typing game
            code or scaning QR Code, which would be given by your quiz master.
          </Text>
        </Row>
      </View>
    </PageWrapper>
  );
}
