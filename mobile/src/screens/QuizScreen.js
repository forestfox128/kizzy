import React, { useState, useEffect } from "react";
import axios from "axios";
import { AsyncStorage, ScrollView } from "react-native";

import { Text, StyleSheet, View, Alert } from "react-native";
import BigButton from "../components/Button";
import Row from "../components/Row";
import AnswerSlot from "../components/AnswerSlot";
import PageWrapper from "../components/PageWrapper";

import { API_URL } from "../utils/API_URL";

const styles = StyleSheet.create({
  viewWrapper: {
    flexGrow: 1,
    flexDirection: "column",
    marginTop: 20
  },
  buttonWrapper: {
    flex: 1
  },
  questionCounter: {
    color: "#112233",
    fontSize: 20,
    padding: 10
  },
  questionStyle: {
    fontSize: 20,
    color: "#112233",
    paddingBottom: 20,
    paddingTop: 10,
    paddingHorizontal: 10
  },
  timer: {
    fontSize: 20,
    color: "#112233",
    paddingVertical: 10
  },
  timerOut: {
    fontSize: 20,
    color: "red",
    paddingVertical: 10
  }
});

export default function QuizScreen({ navigation }) {
  const [chosenAnswer, setChosenAnswer] = useState(-1);
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [multiQuestions, setMultiQuestions] = useState([]);
  const [quizFinished, setQuizFinished] = useState(false);
  const [secondsCounter, setSecondsCounter] = useState(0);
  const [timeOut, setTimeOut] = useState(false);
  const [timer, setTimer] = useState(null);
  const quizID = navigation.getParam("quizID", "0");
  const quizName = navigation.getParam("quizName", "quiz");
  const questionsArray = navigation.getParam("questions", "");
  const [isMulti, setIsMulti] = useState(questionsArray[0].is_multi);

  // TIMER
  onButtonStart = () => {
    let num = 0;
    let timer = setInterval(() => {
      num = num + 1;
      setSecondsCounter(num);
    }, 1000);
    setTimer(timer);
  };

  // clear timer
  onButtonClear = () => {
    clearInterval(timer);
    setTimer(null);
    setSecondsCounter(0);
  };

  retrieveToken = async () => {
    try {
      const value = await AsyncStorage.getItem("token");
      if (value !== null) {
        return value;
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  const timeToTimerFormat = () => {
    const minutes = Math.floor(secondsCounter / 60);
    let seconds = secondsCounter - minutes * 60;
    if (seconds.toString().length == 1) seconds = "0" + seconds;

    return "0" + minutes + ":" + seconds;
  };

  const markQuizAsFinished = async () => {
    onButtonClear();
    const token = await retrieveToken();
    const headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + token
    };
    const data = {};
    axios
      .put(API_URL + "/api/user/quiz/" + quizID + "/finish", data, { headers })
      .then(res => {
        if (res.status === 201) {
          console.log("-------");
          console.log(res.data);
          const allAnswers = res.data.questions;
          const correctAnswers = res.data.correct_answers;
          const overTime = res.data.over_time;
          navigation.navigate("quizEnd", {
            correctAnswers,
            allAnswers,
            overTime
          });
          return true;
        } else {
          console.log("ERROR");
          return false;
        }
      })
      .catch(error => {
        console.log(error);
      });
    return false;
  };

  const putAnswerForQuestion = async () => {
    console.log("SEC COUNTER" + secondsCounter);
    console.log("QUestions" + multiQuestions);
    onButtonClear();
    setTimeOut(false);
    const token = await retrieveToken();
    const headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + token
    };

    const data = {
      question_id: questionsArray[currentQuestion].id,
      answer_id: chosenAnswer,
      time_taken: secondsCounter
    };
    const temp = {
      ...data,
      answers_ids: multiQuestions
    };
    console.log(temp);
    axios
      .put(API_URL + "/api/user/quiz/" + quizID + "/answer", data, { headers })
      .then(res => {
        if (res.status === 201) {
          console.log("Success");
        } else {
          console.log("ERROR");
        }
      })
      .catch(error => {
        console.log(error);
      });
    onButtonStart();
  };

  const nextQuestion = async () => {
    if (chosenAnswer === -1 && !isMulti) {
      Alert.alert("You have to choose one answer");
      return;
    } else if (isMulti && multiQuestions.length === 0) {
      Alert.alert("You have to choose one answer");
      return;
    }

    await putAnswerForQuestion();
    if (currentQuestion + 1 === questionsArray.length) {
      setQuizFinished(true);
    } else {
      setIsMulti(questionsArray[currentQuestion + 1].is_multi);
      setCurrentQuestion(currentQuestion + 1);
      console.log("IS MULTIs" + questionsArray[currentQuestion].is_multi);
      setChosenAnswer(-1);
    }
  };

  const chooseAnswer = id => {
    if (!isMulti) setChosenAnswer(id);
    else {
      if (multiQuestions.includes(id)) {
        const array = multiQuestions.filter(function(item) {
          return item !== id;
        });
        setMultiQuestions(array);
      } else {
        setMultiQuestions([...multiQuestions, id]);
      }
    }
  };

  useEffect(() => {
    onButtonStart();
  }, []);

  return (
    <PageWrapper>
      <View style={styles.viewWrapper}>
        <View style={{ flex: 5 }}>
          <ScrollView>
            <Row justify="left">
              <Text style={[timeOut ? styles.timerOut : styles.timer]}>
                {"" + timeToTimerFormat() + " / " + "01:00"}
              </Text>
            </Row>
            <Row justify="right">
              <Text style={styles.questionCounter}>{quizName}</Text>
              <Text style={styles.questionCounter}>
                {+currentQuestion + 1 + "/" + questionsArray.length}
              </Text>
            </Row>
            <Row justify="center">
              <Text style={styles.questionStyle}>
                {questionsArray[currentQuestion].question}
              </Text>
            </Row>
            {questionsArray[currentQuestion].answers.map(answer => (
              <View>
                <AnswerSlot
                  disabled={quizFinished}
                  clicked={
                    chosenAnswer === answer.id ||
                    multiQuestions.includes(answer.id)
                  }
                  key={answer.id}
                  text={answer.value}
                  onPress={() => chooseAnswer(answer.id)}
                />
              </View>
            ))}
          </ScrollView>
        </View>
        <View style={styles.buttonWrapper}>
          {quizFinished ? (
            <BigButton
              text="Finish Quiz"
              onPress={() => markQuizAsFinished()}
            />
          ) : (
            <BigButton text="Next question" onPress={() => nextQuestion()} />
          )}
        </View>
      </View>
    </PageWrapper>
  );
}
