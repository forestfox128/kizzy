import React from "react";

import { Text, StyleSheet, View } from "react-native";
import BigButton from "../components/Button";
import Row from "../components/Row";
import PageWrapper from "../components/PageWrapper";

const styles = StyleSheet.create({
  viewWrapper: {
    flex: 1,
    flexDirection: "column",
    marginTop: 20
  },
  textSection: {
    flex: 5,
    marginTop: 50
  },
  appNameStyle: {
    fontSize: 30,
    color: "#112233",
    fontWeight: "bold",
    marginTop: 30,
    textAlign: "center"
  },
  scores: {
    fontSize: 40,
    color: "#ff9900",
    fontWeight: "bold",
    marginTop: 30,
    textAlign: "center"
  },
  buttonWrapper: {
    flex: 1
  }
});

export default function QuizEndScreen({ navigation }) {
  const correctAnswers = navigation.getParam("correctAnswers", "0");
  const allQuestions = navigation.getParam("allAnswers", 0);
  const overTime = navigation.getParam("overTime", 0);

  return (
    <PageWrapper>
      <View style={styles.viewWrapper}>
        <View style={styles.textSection}>
          <Row justify="center">
            <Text style={styles.scores}>Congratulations!</Text>
          </Row>
          <Row justify="center">
            <Text style={styles.appNameStyle}>
              You managed to answer correctly to
            </Text>
          </Row>
          <Row justify="center">
            <Text style={styles.scores}>
              {correctAnswers + " / " + allQuestions}
            </Text>
          </Row>
          <Row justify="center">
            <Text style={styles.appNameStyle}>answers.</Text>
          </Row>
          {overTime > 0 && (
            <Row justify="center">
              <Text style={styles.scores}>{overTime + " was over time."}</Text>
            </Row>
          )}
        </View>
        <View style={styles.buttonWrapper}>
          <BigButton
            text="Go to join quiz page"
            onPress={() => navigation.navigate("joinQuiz")}
          />
        </View>
      </View>
    </PageWrapper>
  );
}
