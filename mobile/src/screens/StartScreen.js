import React, { useState, useEffect } from "react";
import axios from "axios";
import { AsyncStorage } from "react-native";

import { Text, StyleSheet, View } from "react-native";
import BigButton from "../components/Button";
import Row from "../components/Row";
import PageWrapper from "../components/PageWrapper";

import { API_URL } from "../utils/API_URL";

const styles = StyleSheet.create({
  viewWrapper: {
    flex: 1,
    flexDirection: "column",
    marginTop: 50
  },
  textSection: {
    flex: 5,
    marginTop: 50
  },
  appNameStyle: {
    fontSize: 30,
    color: "#112233",
    fontWeight: "bold",
    marginTop: 30,
    textAlign: "center"
  },
  scores: {
    fontSize: 40,
    color: "#ff9900",
    fontWeight: "bold",
    marginTop: 30,
    textAlign: "center"
  },
  quizName: {
    color: "#ff9900"
  },
  buttonWrapper: {
    flex: 1
  }
});

export default function StartScreen({ navigation }) {
  const [loading, setLoading] = useState(true);
  const username = navigation.getParam("userNameValue", "XD");
  const [quizName, setQuizName] = useState("quizNAme");
  const [questions, setQuestions] = useState([]);
  const [quizID, setQuizID] = useState(0);

  retrieveToken = async () => {
    try {
      const value = await AsyncStorage.getItem("token");
      if (value !== null) {
        // We have data!!
        return value;
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  const startQuiz = () => {
    navigation.navigate("quiz", { quizName, questions, quizID });
  };

  async function handleGoToQuiz() {
    const token = await retrieveToken();
    const headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + token
    };
    if (loading) {
      axios
        .get(API_URL + "/api/user/quiz", { headers })
        .then(res => {
          if (res.status === 201) {
            console.log(res.data);
            setQuizName(res.data.name);
            setQuestions(res.data.questions);
            setQuizID(res.data.id);
            setLoading(false);
          } else {
            console.log("ERROR");
          }
        })
        .catch(error => {
          console.log(error);
        });
    }
  }
  useEffect(() => {
    handleGoToQuiz();
  }, []);

  return (
    <PageWrapper>
      <View style={styles.viewWrapper}>
        <View style={styles.textSection}>
          <Row justify="center">
            <Text style={styles.scores}>{"Welcome " + username + " !"}</Text>
          </Row>
          <Row justify="center">
            {!loading && (
              <Text style={styles.appNameStyle}>
                {"You've joined "}
                <Text style={styles.quizName}>{quizName}</Text>
                {" quiz. Press start quiz to start quiz."}
              </Text>
            )}
          </Row>
        </View>
        <View style={styles.buttonWrapper}>
          <BigButton text="START QUIZ" onPress={() => startQuiz()} />
        </View>
      </View>
    </PageWrapper>
  );
}
