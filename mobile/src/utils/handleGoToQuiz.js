import axios from "axios";
import { Alert } from "react-native";
import { AsyncStorage } from "react-native";
import { API_URL } from "./API_URL";

storeToken = async token => {
  try {
    await AsyncStorage.setItem("token", token);
  } catch (error) {
    console.log("STORAGE ERROR");
  }
};

export default function handleGoToQuiz(quizToken, navigation) {
  const data = {
    quiz_access_token: quizToken
  };

  axios
    .post(API_URL + "/api/auth/user", data)
    .then(res => {
      if (res.status === 201) {
        storeToken(res.data.token);
        const fields = res.data.fields;
        navigation.navigate("login", { fields });
      } else {
        Alert.alert("Such quiz doesn't exist");
      }
    })
    .catch(error => {
      console.log(error);
      Alert.alert(
        "Sorry, something went wrong. It seems there is no such quiz."
      );
    });
}
