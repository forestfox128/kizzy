import React from "react";
import { StyleSheet, TextInput, View, Text } from "react-native";

const styles = StyleSheet.create({
  textInputStyle: {
    backgroundColor: "#e1e3ea",
    borderRadius: 4,

    shadowRadius: 6,
    color: "#112233",
    fontSize: 16,
    margin: 6,
    height: 54,
    padding: 6,
    paddingHorizontal: 16
  },
  shadowStyle: {
    shadowOffset: { width: 0, height: -1 },
    shadowColor: "#cccccc",
    shadowOpacity: 0.5,
    shadowRadius: 3.84,
    elevation: 5
  }
});

export default function Input({
  placeholder,
  onChangeText,
  isPassword,
  error,
  errorMessage,
  value,
  keyboardType,
  onBlur
}) {
  return (
    <View style={styles.shadowStyle}>
      <TextInput
        keyboardType={keyboardType}
        placeholder={placeholder}
        style={styles.textInputStyle}
        onChangeText={onChangeText}
        secureTextEntry={isPassword}
        placeholderTextColor={"#a5aac0"}
        value={value}
        onBlur={onBlur}
      ></TextInput>
      {error ? <Text>{errorMessage}</Text> : <></>}
    </View>
  );
}
