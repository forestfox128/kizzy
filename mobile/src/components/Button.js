import React from "react";
import { StyleSheet, TouchableOpacity, View, Text } from "react-native";

const styles = StyleSheet.create({
  buttonStyle: {
    backgroundColor: "#f0f1f4",
    borderRadius: 4,
    borderWidth: 4,
    borderColor: "#112233",
    margin: 6,
    height: 54,
    padding: 6,
    alignItems: "center",
    padding: 10
  },
  buttonStyleReverse: {
    backgroundColor: "#112233",
    borderRadius: 4,
    borderWidth: 4,
    borderColor: "#112233",
    margin: 6,
    height: 54,
    padding: 6,
    alignItems: "center",
    padding: 10
  },
  buttonText: {
    fontSize: 20,
    color: "#112233",
    fontWeight: "bold"
  },
  buttonTextReverse: {
    fontSize: 20,
    color: "#f0f1f4",
    fontWeight: "bold"
  },
  shadowStyle: {
    // borderWidth: 2,
    // borderColor: "#ff8533",
    shadowColor: "#ffb380",
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.5,
    shadowRadius: 3.84,
    elevation: 5,
    height: 54
  }
});

export default function BigButton({ onPress, text, color, reverse }) {
  return (
    <View style={styles.shadowStyle}>
      <TouchableOpacity
        style={[reverse ? styles.buttonStyleReverse : styles.buttonStyle]}
        onPress={onPress}
      >
        <Text style={reverse ? styles.buttonTextReverse : styles.buttonText}>
          {text}
        </Text>
      </TouchableOpacity>
    </View>
  );
}
