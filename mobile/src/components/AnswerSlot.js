import React from "react";
import { StyleSheet, TouchableOpacity, View, Text } from "react-native";

const styles = StyleSheet.create({
  aswerCustom: {
    borderRadius: 4,
    borderWidth: 1,
    margin: 6,
    height: 54,
    alignItems: "center",
    padding: 12
  },
  answerChoosen: {
    backgroundColor: "#ff9900",
    borderColor: "#ff9900"
  },
  answer: {
    backgroundColor: "#f0f1f4",
    borderColor: "#112233"
  },
  buttonText: {
    fontSize: 16,
    color: "#112233"
  }
});

export default function AnswerSlot({ clicked, text, onPress, disabled }) {
  return (
    <TouchableOpacity
      style={[
        styles.aswerCustom,
        clicked ? styles.answerChoosen : styles.answer
      ]}
      onPress={onPress}
      disabled={disabled}
    >
      <Text style={[styles.buttonText, clicked ? { color: "#ffffff" } : {}]}>
        {text}
      </Text>
    </TouchableOpacity>
  );
}
