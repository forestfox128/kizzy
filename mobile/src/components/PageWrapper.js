import React from "react";
import { StyleSheet, ImageBackground } from "react-native";

const styles = StyleSheet.create({});

export default function PageWrapper({ children }) {
  return (
    <ImageBackground
      source={require("../../assets/background.png")}
      style={{ flex: 1 }}
    >
      {children}
    </ImageBackground>
  );
}
