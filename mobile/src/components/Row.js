import React from "react";
import { StyleSheet, View } from "react-native";

const styles = StyleSheet.create({
  baseRow: {
    flexDirection: "row"
  },
  left: {
    justifyContent: "flex-start"
  },
  right: {
    justifyContent: "flex-end"
  },
  center: {
    justifyContent: "center"
  }
});

export default function Row({ children, style, justify }) {
  let justifyStyle = styles.left;
  switch (justify) {
    case "right":
      justifyStyle = styles.right;
      break;
    case "center":
      justifyStyle = styles.center;
      break;
    default:
      justifyStyle = styles.left;
  }
  return <View style={[styles.baseRow, style, justifyStyle]}>{children}</View>;
}
