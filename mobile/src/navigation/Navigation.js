import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

import JoinQuizScreen from "../screens/JoinQuizScreen";
import LoginScreen from "../screens/LoginScreen";
import QuizScreen from "../screens/QuizScreen";
import QuizEndScreen from "../screens/QuizEndScreen";
import StartScreen from "../screens/StartScreen";
import QRScanner from "../screens/QRScanner";

const MainNavigator = createStackNavigator(
  {
    joinQuiz: { screen: JoinQuizScreen },
    login: { screen: LoginScreen },
    quizStart: { screen: StartScreen },
    quiz: { screen: QuizScreen },
    scanner: { screen: QRScanner },
    quizEnd: { screen: QuizEndScreen }
  },
  { headerMode: "none", initialRouteName: "joinQuiz" }
);

const Navigation = createAppContainer(MainNavigator);

export default Navigation;
