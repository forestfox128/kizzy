import React from "react";
import * as Font from "expo-font";
import { StyleSheet, View, Text, ImageBackground } from "react-native";
import Navigation from "./src/navigation/Navigation";

const styles = StyleSheet.create({
  screenView: {
    padding: 10,
    flex: 1
  }
});

export default class App extends React.Component {
  render() {
    return <Navigation />;
  }
}
