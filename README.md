# Kizzy - Pop quiz made simple

## Opis projektu
Kizzy - to aplikacja pozwalająca na prowadzenie quizów poprzez aplikację mobilną.
Przydatna zarówno podczas imprezowych zabaw wsród znajomych, jak i dla nauczycieli czy 
wykładowców podczas robienia tzw. `wejściówek`. Aplikacja pozwala uczestniczyć uzytkownikom poprzez najłatwiejszą w dzisiejszych czasach droge interakcji z aplikacjami, czyli poprzez smartfona.
Aplikacja pozwala na stworzenie quizu poprzez panel administratora dostepny z poziomu aplikacji webowej oraz udostepnienie quizu, poprzez specjalny kod dla `graczy` korzystających z aplikacji mobilnej.
Po wczytaniu kodu, gracz musi podać swój nick i dołącza do quizu. Po rozwiązaniu quizu wyniki poszczególnych graczy są dostępne w aplikacji webowej.

## Wymagania użytkownika

### Sprint 1
- Panel administracyjny (aplikacja webowa) pozwalający na tworzenie nowych quizów oraz wystawiania ich na serwerze.
- Aplikacja mobilna pozwalająca na dołączenie do quizu (poprzez jego unikalny kod, bądź tez kod QR), wpisanie nicku oraz rozwiązanie quizu i przesłanie rozwiązania na serwer.
- Wyniki quizów dostępne w formie łatwych do przenalizowania danych.
- Serwer obsługujący dodawanie nowego quizu, zwrócenie pytań, sprawdzanie prawidłowych odpowiedzi i przesłanie wyników quizów

### Sprint 2
- Możliwość tworzenia pytań czasowych, tzw. ograniczenie czasu na udzielenie odpowiedzi na zadane pytanie do X sekund/minut.
- Możliwość wprowadzenia walidacji przez zarządzającego quizem.
- Możliwość tworzenia pytań wielokrotnego wyboru.
- Możliwość definiowania przez zarządzającego quizem, dodatkowych danych jakie musi podać dołączający do quizu. 

