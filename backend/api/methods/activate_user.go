package methods

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"kizzy/api/objects"
	"kizzy/api/utils"
	"kizzy/models"
	"log"
	"net/http"
	"strings"
)

type ActivateUserRoute struct {
}

type ActivateUserPayload struct {
	Name               string                     `json:"name"`
	RegistrationFields objects.RegistrationFields `json:"fields,omitempty"`
}

type ActivateUserResponse struct {
	Success bool `json:"success"`
}

func NewActivateUserRoute() *ActivateUserRoute {
	return &ActivateUserRoute{}
}

func (r *ActivateUserRoute) Payload() interface{} {
	return &ActivateUserPayload{}
}

func (r *ActivateUserRoute) validate(payload interface{}) (*ActivateUserPayload, error) {
	reqPayload, ok := payload.(*ActivateUserPayload)
	if !ok {
		return nil, errors.New("Invalid type assertion")
	}
	if reqPayload.Name == "" {
		return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Name is required")
	}
	for _, registraionField := range reqPayload.RegistrationFields {
		if registraionField.FieldType == "" {
			return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Field type in fields is required")
		}
		if !objects.IsValidRegistrationField(registraionField.FieldType) {
			return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Wrong field type in fields")
		}
		if registraionField.Value == "" {
			return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Field value could not be empty")
		}
	}
	return reqPayload, nil
}

func (r *ActivateUserRoute) validateFields(receivedFields objects.RegistrationFields, quizFields []*models.RegistrationFormField) ([]*models.RegisteredFormField, error) {
	requiredFields := make(map[string]*models.RegistrationFormField)
	optionalFields := make(map[string]*models.RegistrationFormField)
	validatedFields := make([]*models.RegisteredFormField, 0, len(quizFields))

	for _, quizField := range quizFields {
		if quizField.Required {
			requiredFields[quizField.Name] = quizField
		} else {
			optionalFields[quizField.Name] = quizField
		}
	}

	for _, receivedField := range receivedFields {
		requiredField, existsRequired := requiredFields[receivedField.Name]
		optionalField, existsOptional := optionalFields[receivedField.Name]

		var currentField *models.RegistrationFormField

		if existsRequired {
			if requiredField.IsSet {
				return nil, utils.NewApiError(http.StatusUnprocessableEntity, fmt.Sprintf("Duplicated required field `%s`", receivedField.Name))
			}
			requiredField.IsSet = true
			currentField = requiredField
		} else if existsOptional {
			if optionalField.IsSet {
				return nil, utils.NewApiError(http.StatusUnprocessableEntity, fmt.Sprintf("Duplicated optional field `%s`", receivedField.Name))
			}
			optionalField.IsSet = true
			currentField = optionalField
		} else {
			return nil, utils.NewApiError(http.StatusUnprocessableEntity, fmt.Sprintf("Field `%s` is invalid for given field", receivedField.Name))
		}

		if currentField.Type != receivedField.FieldType {
			return nil, utils.NewApiError(http.StatusUnprocessableEntity, fmt.Sprintf("Field `%s` type missmatch", receivedField.Name))
		}

		newModelField := &models.RegisteredFormField{
			Type: receivedField.FieldType,
			Name: receivedField.Name,
		}

		switch receivedField.FieldType {
		case "text":
			text, ok := receivedField.Value.(string)
			if !ok {
				return nil, utils.NewApiError(http.StatusUnprocessableEntity, fmt.Sprintf("Field `%s` has invalid value in given form, expects string", receivedField.Name))
			}

			newModelField.ValueString = text
		case "select", "dropdown":
			text, ok := receivedField.Value.(string)
			if !ok {
				return nil, utils.NewApiError(http.StatusUnprocessableEntity, fmt.Sprintf("Field `%s` has invalid value in given form, expects string", receivedField.Name))
			}
			if !currentField.ValidateValue(text) {
				if receivedField.FieldType == "select" {
					return nil, utils.NewApiError(http.StatusUnprocessableEntity, fmt.Sprintf("Field `%s` should be one of: %s", receivedField.Name, strings.Join(currentField.Values, ", ")))
				} else {
					return nil, utils.NewApiError(http.StatusUnprocessableEntity, fmt.Sprintf("Field `%s` is not allowed value defined by admin", receivedField.Name))
				}
			}

			newModelField.ValueString = text
		case "email":
			email, ok := receivedField.Value.(string)
			if !ok {
				return nil, utils.NewApiError(http.StatusUnprocessableEntity, fmt.Sprintf("Field `%s` has invalid value in given form, expects email", receivedField.Name))
			}
			if !strings.Contains(email, "@") {
				return nil, utils.NewApiError(http.StatusUnprocessableEntity, fmt.Sprintf("Field `%s` has invalid value in given form, expects email", receivedField.Name))
			}
			newModelField.ValueString = email
		case "number":
			number, ok := receivedField.Value.(float64)
			if !ok {
				return nil, utils.NewApiError(http.StatusUnprocessableEntity, fmt.Sprintf("Field `%s` has invalid value in given form, expects integer", receivedField.Name))
			}

			newModelField.ValueInt = int(number)
		}

		validatedFields = append(validatedFields, newModelField)
	}

	for name, field := range requiredFields {
		if !field.IsSet {
			return nil, utils.NewApiError(http.StatusBadRequest, fmt.Sprintf("Field `%s` is required", name))
		}
	}
	return validatedFields, nil
}

func (r *ActivateUserRoute) Do(ctx context.Context, payload interface{}) (*utils.ApiResponse, error) {
	reqPayload, err := r.validate(payload)
	if err != nil {
		return nil, err
	}
	token, ok := utils.GetTokenFromContext(ctx).(*models.Token)
	if !ok {
		return nil, errors.New("Could not get token from ctx")
	}

	user, err := models.GetUser(token.UserId)
	if err != nil {
		return nil, err
	}
	if user.Active {
		return nil, utils.NewApiError(http.StatusConflict, "User is already active")
	}

	quiz, err := models.GetQuiz(user.QuizID)
	if err != nil {
		return nil, err
	}

	if !quiz.Started {
		return nil, utils.NewApiError(http.StatusNotFound, "No Quiz found for given access token")
	}
	if quiz.Finished {
		return nil, utils.NewApiError(http.StatusBadRequest, "Quiz already finished")
	}

	validatedForm, err := r.validateFields(reqPayload.RegistrationFields, quiz.RegistrationFormFields)
	if err != nil {
		return nil, err
	}

	userGeo, userIP := utils.GetGeoLocFromContext(ctx)

	if err := user.ActivateUser(reqPayload.Name, validatedForm, userGeo, userIP); err != nil {
		log.Println(err.Error())
		return nil, utils.NewApiError(http.StatusInternalServerError, "Could not activate user")
	}

	marshaledResp, err := json.Marshal(ActivateUserResponse{
		Success: true,
	})
	if err != nil {
		return nil, err
	}

	return &utils.ApiResponse{
		StatusCode: http.StatusCreated,
		Response:   marshaledResp,
	}, nil
}
