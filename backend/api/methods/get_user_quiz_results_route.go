package methods

import (
	"context"
	"encoding/json"
	"errors"
	"kizzy/api/objects"
	"kizzy/api/utils"
	"kizzy/models"
	"log"
	"net/http"
)

type GetUserQuizResultsRoute struct {
}

type GetUserQuizResultsPayload struct {
}

type GetUserQuizResultsResponse struct {
	objects.UserQuizResultObject
}

func NewGetUserQuizResultsRoute() *GetUserQuizResultsRoute {
	return &GetUserQuizResultsRoute{}
}

func (r *GetUserQuizResultsRoute) Payload() interface{} {
	return &GetUserQuizResultsPayload{}
}

func (r *GetUserQuizResultsRoute) generateAnswerReport(quizModel *models.Quiz, userAnswers []*models.UserAnswer) objects.UserAnswersResultObject {
	userAnswersMapped := make(map[uint]*objects.UserAnswerResultObject)
	answersResults := make(objects.UserAnswersResultObject, 0, len(quizModel.Questions))
	for _, userAnswer := range userAnswers {
		userAnswersMapped[userAnswer.QuestionID] = &objects.UserAnswerResultObject{
			AnswerID:   userAnswer.AnswerID,
			AnswerTime: userAnswer.TimeTaken,
			AnswerIDs:  userAnswer.GetAnswersIDs(),
		}
	}
	for _, question := range quizModel.Questions {
		userAnswerResult := &objects.UserAnswerResultObject{
			Question: question.Value,
			IsMulti:  question.IsMulti,
		}
		userAnswer, exists := userAnswersMapped[question.ID]
		if !exists {
			userAnswerResult.Answer = "-"
			userAnswerResult.IsAnswerCorrect = false
			if question.QuestionTime != 0 {
				userAnswerResult.IsAnswerOverTime = question.QuestionTime < userAnswer.AnswerTime
			}
		}
		log.Println(question.ID)
		log.Printf("%+v/n", userAnswer)
		if !question.IsMulti {
			for _, answerModel := range question.Answers {
				if answerModel.IsCorrect {
					if exists && userAnswer.AnswerID == answerModel.ID {
						userAnswerResult.IsAnswerCorrect = true
						userAnswerResult.Answer = answerModel.Value
					} else {
						userAnswerResult.CorrectAnswer = answerModel.Value
					}
				} else if exists && answerModel.ID == userAnswer.AnswerID {
					userAnswerResult.Answer = answerModel.Value
				}
			}
		} else {
			answeredProperly := 0
			answeredImproperly := 0
			properAnswers := 0
			userAnswers := make([]string, 0, 0)
			correctAnswers := make([]string, 0, 0)
			for _, answerModel := range question.Answers {
				found := false
				if exists {
					for _, userAnswerMultiID := range userAnswer.AnswerIDs {
						if userAnswerMultiID == userAnswer.AnswerID {
							found = true
						}
					}
				}
				if found {
					userAnswers = append(userAnswers, answerModel.Value)
					if answerModel.IsCorrect {
						answeredProperly++
					}
				} else {
					if answerModel.IsCorrect {
						answeredImproperly++
						correctAnswers = append(correctAnswers, answerModel.Value)
					} else if exists {
						answeredProperly++
					} else {
						answeredImproperly++
					}
				}

			}
			if answeredImproperly == 0 && answeredProperly == properAnswers {
				userAnswerResult.IsAnswerCorrect = true
			} else {
				userAnswerResult.IsAnswerCorrect = false
			}

			userAnswerResult.Answers = userAnswers
			userAnswerResult.CorrectAnswers = correctAnswers

		}
		answersResults = append(answersResults, userAnswerResult)

	}

	return answersResults
}

func (r *GetUserQuizResultsRoute) Do(ctx context.Context, payload interface{}) (*utils.ApiResponse, error) {
	token, ok := utils.GetTokenFromContext(ctx).(*models.Token)
	if !ok {
		return nil, errors.New("Could not get token from ctx")
	}

	user, err := models.GetUser(token.UserId)
	if err != nil {
		return nil, err
	}
	if !user.Active {
		return nil, utils.NewApiError(http.StatusConflict, "User has to be activated first")
	}
	if !user.HasFinishedQuiz {
		return nil, utils.NewApiError(http.StatusConflict, "User has to finish quiz first")
	}

	quiz, err := models.GetQuiz(user.QuizID)
	if err != nil {
		return nil, err
	}

	if !quiz.Started {
		return nil, utils.NewApiError(http.StatusNotFound, "There is no results avaliable")
	}
	if !quiz.Finished {
		return nil, utils.NewApiError(http.StatusConflict, "Quiz has to end first")
	}
	quizObject := quiz.GetObjectFromModel(true)

	quizResult := objects.UserQuizResultObject{
		ID:           quizObject.ID,
		Name:         quizObject.Name,
		CreationDate: quizObject.CreationDate,
		FinishDate:   quizObject.FinishDate,
		Answers:      r.generateAnswerReport(quiz, user.UserAnswers),
	}

	marshaledResp, err := json.Marshal(GetUserQuizResultsResponse{UserQuizResultObject: quizResult})
	if err != nil {
		return nil, err
	}

	return &utils.ApiResponse{
		StatusCode: http.StatusCreated,
		Response:   marshaledResp,
	}, nil
}
