package methods

import (
	"context"
	"encoding/json"
	"errors"
	"kizzy/api/objects"
	"kizzy/api/utils"
	"kizzy/models"
	"net/http"
)

type ListQuizRoute struct {
}

type ListQuizPayload struct {
}

type ListQuizResponse struct {
	Quizzes []*objects.QuizObject `json:"quizzes"`
}

func NewListQuizRoute() *ListQuizRoute {
	return &ListQuizRoute{}
}

func (r *ListQuizRoute) Payload() interface{} {
	return &ListQuizPayload{}
}

func (r *ListQuizRoute) Do(ctx context.Context, payload interface{}) (*utils.ApiResponse, error) {
	token, ok := utils.GetTokenFromContext(ctx).(*models.Token)
	if !ok {
		return nil, errors.New("Could not get token from ctx")
	}

	quizzes, err := models.GetQuizzesForAdmin(token.UserId)
	if err != nil {
		return nil, err
	}

	quizzesObjects := make([]*objects.QuizObject, 0, len(quizzes))
	for _, quiz := range quizzes {
		quizzesObjects = append(quizzesObjects, quiz.GetObjectFromModel(false))
	}

	response := &ListQuizResponse{Quizzes: quizzesObjects}

	marshaledResp, err := json.Marshal(response)
	if err != nil {
		return nil, err
	}

	return &utils.ApiResponse{
		StatusCode: http.StatusOK,
		Response:   marshaledResp,
	}, nil
}
