package methods

import (
	"context"
	"encoding/json"
	"errors"
	"kizzy/api/objects"
	"kizzy/api/utils"
	"kizzy/models"
	"log"
	"net/http"
	"strconv"
)

type GetQuizUserReportRoute struct {
}

type GetQuizUserReportPayload struct {
}

type GetQuizUserReportResponse struct {
	objects.UserQuizReportObject
}

func NewGetUserReportRoute() *GetQuizUserReportRoute {
	return &GetQuizUserReportRoute{}
}

func (r *GetQuizUserReportRoute) Payload() interface{} {
	return &GetQuizUserReportPayload{}
}

func (r *GetQuizUserReportRoute) validate(ctx context.Context) (uint, uint, error) {
	vars, ok := utils.GetVarsFromContext(ctx).(map[string]string)
	if !ok {
		return 0, 0, errors.New("Could not get vars from ctx")
	}

	quizVar, ok := vars["quiz"]
	if !ok || quizVar == "" {
		return 0, 0, utils.NewApiError(http.StatusUnprocessableEntity, "Missing quiz id in path")
	}
	tmpQuiz, err := strconv.ParseUint(quizVar, 10, 32)
	if err != nil {
		return 0, 0, utils.NewApiError(http.StatusUnprocessableEntity, "Quiz id should be int")
	}

	userVar, ok := vars["user"]
	if !ok || userVar == "" {
		return 0, 0, utils.NewApiError(http.StatusUnprocessableEntity, "Missing quiz id in path")
	}
	tmpUser, err := strconv.ParseUint(userVar, 10, 32)
	if err != nil {
		return 0, 0, utils.NewApiError(http.StatusUnprocessableEntity, "Quiz id should be int")
	}

	return uint(tmpQuiz), uint(tmpUser), nil
}

func (r *GetQuizUserReportRoute) generateAnswerReport(quizModel *models.Quiz, userAnswers []*models.UserAnswer) objects.UserAnswersResultObject {
	userAnswersMapped := make(map[uint]*objects.UserAnswerResultObject)
	answersResults := make(objects.UserAnswersResultObject, 0, len(quizModel.Questions))
	for _, userAnswer := range userAnswers {
		userAnswersMapped[userAnswer.QuestionID] = &objects.UserAnswerResultObject{
			AnswerID:   userAnswer.AnswerID,
			AnswerTime: userAnswer.TimeTaken,
			AnswerIDs:  userAnswer.GetAnswersIDs(),
		}
	}
	for _, question := range quizModel.Questions {
		userAnswerResult := &objects.UserAnswerResultObject{
			Question: question.Value,
			IsMulti:  question.IsMulti,
		}
		userAnswer, exists := userAnswersMapped[question.ID]
		if !exists {
			userAnswerResult.Answer = "-"
			userAnswerResult.IsAnswerCorrect = false
			if question.QuestionTime != 0 {
				userAnswerResult.IsAnswerOverTime = question.QuestionTime < userAnswer.AnswerTime
			}
		}
		log.Println(question.ID)
		log.Printf("%+v/n", userAnswer)
		if !question.IsMulti {
			for _, answerModel := range question.Answers {
				if answerModel.IsCorrect {
					if exists && userAnswer.AnswerID == answerModel.ID {
						userAnswerResult.IsAnswerCorrect = true
						userAnswerResult.Answer = answerModel.Value
					} else {
						userAnswerResult.CorrectAnswer = answerModel.Value
					}
				} else if exists && answerModel.ID == userAnswer.AnswerID {
					userAnswerResult.Answer = answerModel.Value
				}
			}
		} else {
			answeredProperly := 0
			answeredImproperly := 0
			properAnswers := 0
			userAnswers := make([]string, 0, 0)
			correctAnswers := make([]string, 0, 0)
			for _, answerModel := range question.Answers {
				found := false
				if exists {
					for _, userAnswerMultiID := range userAnswer.AnswerIDs {
						if userAnswerMultiID == userAnswer.AnswerID {
							found = true
						}
					}
				}
				if found {
					userAnswers = append(userAnswers, answerModel.Value)
					if answerModel.IsCorrect {
						answeredProperly++
					}
				} else {
					if answerModel.IsCorrect {
						answeredImproperly++
						correctAnswers = append(correctAnswers, answerModel.Value)
					} else if exists {
						answeredProperly++
					} else {
						answeredImproperly++
					}
				}

			}
			if answeredImproperly == 0 && answeredProperly == properAnswers {
				userAnswerResult.IsAnswerCorrect = true
			} else {
				userAnswerResult.IsAnswerCorrect = false
			}

			userAnswerResult.Answers = userAnswers
			userAnswerResult.CorrectAnswers = correctAnswers

		}
		answersResults = append(answersResults, userAnswerResult)

	}

	return answersResults
}

func (r *GetQuizUserReportRoute) Do(ctx context.Context, payload interface{}) (*utils.ApiResponse, error) {
	token, ok := utils.GetTokenFromContext(ctx).(*models.Token)
	if !ok {
		return nil, errors.New("Could not get token from ctx")
	}

	quizID, userID, err := r.validate(ctx)
	if err != nil {
		return nil, err
	}

	admin, err := models.GetAdmin(token.UserId)
	if err != nil {
		return nil, err
	}

	quiz, err := models.GetQuiz(quizID)
	if err != nil {
		return nil, err
	}
	if quiz.AdminID != admin.ID {
		return nil, utils.NewApiError(http.StatusForbidden, "You have no access for this quiz")
	}

	user, err := models.GetUser(userID)
	if err != nil {
		return nil, err
	}

	respObject := objects.UserQuizReportObject{
		User:        user.GenerateUserObject(),
		UserAnswers: r.generateAnswerReport(quiz, user.UserAnswers),
	}

	marshaledResp, err := json.Marshal(GetQuizUserReportResponse{
		UserQuizReportObject: respObject,
	})
	if err != nil {
		return nil, err
	}

	return &utils.ApiResponse{
		StatusCode: http.StatusOK,
		Response:   marshaledResp,
	}, nil
}
