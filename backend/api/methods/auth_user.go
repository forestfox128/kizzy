package methods

import (
	"context"
	"encoding/json"
	"errors"
	"kizzy/api/objects"
	"kizzy/api/utils"
	"kizzy/models"
	"net/http"
)

type AuthUserRoute struct {
}

type AuthUserPayload struct {
	AccessToken string `json:"quiz_access_token"`
}

type AuthUserResponse struct {
	Token              string                     `json:"token"`
	RegistrationFields objects.RegistrationFields `json:"fields"`
}

func NewAuthUserRoute() *AuthUserRoute {
	return &AuthUserRoute{}
}

func (r *AuthUserRoute) Payload() interface{} {
	return &AuthUserPayload{}
}

func (r *AuthUserRoute) validate(payload interface{}) (*AuthUserPayload, error) {
	reqPayload, ok := payload.(*AuthUserPayload)
	if !ok {
		return nil, errors.New("Invalid type assertion")
	}
	if reqPayload.AccessToken == "" {
		return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Access token is required")
	}
	return reqPayload, nil
}

func (r *AuthUserRoute) Do(ctx context.Context, payload interface{}) (*utils.ApiResponse, error) {
	reqPayload, err := r.validate(payload)
	if err != nil {
		return nil, err
	}
	newUser, err := models.CreateUser(reqPayload.AccessToken)
	if err != nil {
		return nil, err
	}

	quiz, err := models.GetQuiz(newUser.QuizID)
	if err != nil {
		return nil, err
	}

	if !quiz.Started {
		return nil, utils.NewApiError(http.StatusNotFound, "No Quiz found for given access token")
	}
	if quiz.Finished {
		return nil, utils.NewApiError(http.StatusBadRequest, "Quiz already finished")
	}

	registrationFields := make(objects.RegistrationFields, 0, len(quiz.RegistrationFormFields))

	for _, registraionField := range quiz.RegistrationFormFields {
		registrationFields = append(registrationFields, &objects.RegistrationField{
			FieldType: registraionField.Type,
			Name:      registraionField.Name,
			Required:  registraionField.Required,
		})
	}

	marshaledResp, err := json.Marshal(AuthUserResponse{
		RegistrationFields: registrationFields,
		Token:              newUser.Token,
	})
	if err != nil {
		return nil, err
	}

	return &utils.ApiResponse{
		StatusCode: http.StatusCreated,
		Response:   marshaledResp,
	}, nil
}
