package methods

import (
	"context"
	"encoding/json"
	"errors"
	"kizzy/api/objects"
	"kizzy/api/utils"
	"kizzy/models"
	"net/http"
	"strconv"
)

type UpdateQuizAnswerRoute struct {
}

type UpdateQuizAnswerPayload struct {
	objects.UserAnswerObject
}

type UpdateQuizAnswerResponse struct {
	UserAnswers objects.UserAnswersObject `json:"answers"`
}

func NewUpdateQuizAnswerRoute() *UpdateQuizAnswerRoute {
	return &UpdateQuizAnswerRoute{}
}

func (r *UpdateQuizAnswerRoute) Payload() interface{} {
	return &UpdateQuizAnswerPayload{}
}

func (r *UpdateQuizAnswerRoute) validate(payload interface{}, vars map[string]string) (*UpdateQuizAnswerPayload, uint, error) {
	quizVar, exists := vars["quiz"]
	if !exists {
		return nil, 0, utils.NewApiError(http.StatusBadRequest, "Missing `quiz_id` in path")
	}
	quizID, err := strconv.ParseUint(quizVar, 10, 32)
	if err != nil {
		return nil, 0, utils.NewApiError(http.StatusUnprocessableEntity, "Quiz id should be int")
	}

	reqPayload, ok := payload.(*UpdateQuizAnswerPayload)
	if !ok {
		return nil, 0, errors.New("Invalid type assertion")
	}

	if reqPayload.QuestionID == 0 {
		return nil, 0, utils.NewApiError(http.StatusBadRequest, "Missing or incorrect `question_id`")
	}
	if reqPayload.AnswerID == 0 && reqPayload.AnswerIDs == nil {
		return nil, 0, utils.NewApiError(http.StatusBadRequest, "Missing or incorrect `answer_id`")
	}
	if reqPayload.AnswerIDs != nil {
		for _, id := range reqPayload.AnswerIDs {
			if id == 0 {
				return nil, 0, utils.NewApiError(http.StatusBadRequest, "Missing or incorrect `answer_id` ins `answer_ids`")
			}
		}
	}
	return reqPayload, uint(quizID), nil
}

func (r *UpdateQuizAnswerRoute) getDesiredQuestion(reqPayload *UpdateQuizAnswerPayload, questions []*models.Question) (*models.Question, error) {
	var selectedQuestion *models.Question
	for _, question := range questions {
		if question.ID == reqPayload.QuestionID {
			selectedQuestion = question
			break
		}
	}
	if selectedQuestion == nil {
		return nil, utils.NewApiError(http.StatusBadRequest, "Wrong `question_id`")
	}

	return selectedQuestion, nil
}

func (r *UpdateQuizAnswerRoute) checkIfAnswerExists(answerID uint, selectedQuestion *models.Question) error {
	var selectedAnswer *models.Answer

	for _, answer := range selectedQuestion.Answers {
		if answer.ID == answerID {
			selectedAnswer = answer
			break
		}
	}
	if selectedAnswer == nil {
		return utils.NewApiError(http.StatusBadRequest, "Wrong `answer_id`")
	}

	return nil
}

func (r *UpdateQuizAnswerRoute) Do(ctx context.Context, payload interface{}) (*utils.ApiResponse, error) {
	token, ok := utils.GetTokenFromContext(ctx).(*models.Token)
	if !ok {
		return nil, errors.New("Could not get token from ctx")
	}
	vars, ok := utils.GetVarsFromContext(ctx).(map[string]string)
	if !ok {
		return nil, errors.New("Could not get vars from ctx")
	}
	reqPayload, quizID, err := r.validate(payload, vars)
	if err != nil {
		return nil, err
	}

	user, err := models.GetUser(token.UserId)
	if err != nil {
		return nil, err
	}
	if !user.Active {
		return nil, utils.NewApiError(http.StatusConflict, "User has to be activated first")
	}
	if user.HasFinishedQuiz {
		return nil, utils.NewApiError(http.StatusConflict, "User can't answer to quiz that he already finished")
	}

	quiz, err := models.GetQuiz(quizID)
	if err != nil {
		return nil, err
	}

	if quiz.ID != user.QuizID {
		return nil, utils.NewApiError(http.StatusForbidden, "You have no access for this quiz")
	}

	if !quiz.Started {
		return nil, utils.NewApiError(http.StatusNotFound, "Quiz not found")
	}
	if quiz.Finished {
		return nil, utils.NewApiError(http.StatusNotFound, "Quiz is already finished")
	}

	selectedQuestion, err := r.getDesiredQuestion(reqPayload, quiz.Questions)
	if err != nil {
		return nil, err
	}

	var answersObject objects.UserAnswersObject

	if selectedQuestion.IsMulti {
		for _, answerID := range reqPayload.AnswerIDs {
			if err := r.checkIfAnswerExists(answerID, selectedQuestion); err != nil {
				return nil, err
			}
		}
	} else {
		if err := r.checkIfAnswerExists(reqPayload.AnswerID, selectedQuestion); err != nil {
			return nil, err
		}
	}

	answersObject, err = user.UpdateAnswer(reqPayload.UserAnswerObject, selectedQuestion.IsMulti)
	if err != nil {
		return nil, err
	}

	marshaledResp, err := json.Marshal(UpdateQuizAnswerResponse{
		UserAnswers: answersObject,
	})
	if err != nil {
		return nil, err
	}

	return &utils.ApiResponse{
		StatusCode: http.StatusCreated,
		Response:   marshaledResp,
	}, nil
}
