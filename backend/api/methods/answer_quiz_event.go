package methods

import (
	"context"
	"encoding/json"
	"errors"
	"kizzy/api/utils"
	"kizzy/models"
	"net/http"
	"strconv"
)

type AnswerValidationEventRoute struct {
}

type AnswerValidationEventPayload struct {
	EventID uint   `json:"event_id"`
	Answer  string `json:"answer"`
}

type AnswerValidationEventResponse struct {
	Success bool `json:"success"`
}

func NewAnswerValidationEventRoute() *AnswerValidationEventRoute {
	return &AnswerValidationEventRoute{}
}

func (r *AnswerValidationEventRoute) Payload() interface{} {
	return &AnswerValidationEventPayload{}
}

func (r *AnswerValidationEventRoute) validate(vars map[string]string, payload interface{}) (*AnswerValidationEventPayload, uint, error) {
	quizVar, exists := vars["quiz"]
	if !exists {
		return nil, 0, utils.NewApiError(http.StatusBadRequest, "Missing `quiz_id` in path")
	}
	quizID, err := strconv.ParseUint(quizVar, 10, 32)
	if err != nil {
		return nil, 0, utils.NewApiError(http.StatusUnprocessableEntity, "Quiz id should be int")
	}

	reqPayload, ok := payload.(*AnswerValidationEventPayload)
	if !ok {
		return nil, 0, errors.New("Invalid type assertion")
	}

	if reqPayload.Answer == "" {
		return nil, 0, utils.NewApiError(http.StatusBadRequest, "`answer` can't be empty")
	}
	if reqPayload.EventID == 0 {
		return nil, 0, utils.NewApiError(http.StatusBadRequest, "`event_id` can't be empty or `0`")
	}

	return reqPayload, uint(quizID), nil
}

func (r *AnswerValidationEventRoute) Do(ctx context.Context, payload interface{}) (*utils.ApiResponse, error) {
	token, ok := utils.GetTokenFromContext(ctx).(*models.Token)
	if !ok {
		return nil, errors.New("Could not get token from ctx")
	}
	vars, ok := utils.GetVarsFromContext(ctx).(map[string]string)
	if !ok {
		return nil, errors.New("Could not get vars from ctx")
	}
	reqPayload, quizID, err := r.validate(vars, payload)
	if err != nil {
		return nil, err
	}

	user, err := models.GetUser(token.UserId)
	if err != nil {
		return nil, err
	}
	if !user.Active {
		return nil, utils.NewApiError(http.StatusConflict, "User has to be activated first")
	}
	if user.HasFinishedQuiz {
		return nil, utils.NewApiError(http.StatusConflict, "User is already finished quiz")
	}

	quiz, err := models.GetQuiz(quizID)
	if err != nil {
		return nil, err
	}

	if quiz.ID != user.QuizID {
		return nil, utils.NewApiError(http.StatusForbidden, "You have no access for this quiz")
	}

	isCorrect, err := quiz.ValidateEvent(reqPayload.EventID, reqPayload.Answer)
	if err != nil {
		return nil, err
	}

	if err := user.AddValidationAnswer(reqPayload.EventID, reqPayload.Answer, isCorrect); err != nil {
		return nil, err
	}

	marshaledResp, err := json.Marshal(AnswerValidationEventResponse{
		Success: true,
	})
	if err != nil {
		return nil, err
	}

	return &utils.ApiResponse{
		StatusCode: http.StatusCreated,
		Response:   marshaledResp,
	}, nil
}
