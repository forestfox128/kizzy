package methods

import (
	"context"
	"encoding/json"
	"errors"
	"kizzy/api/utils"
	"kizzy/models"
	"net/http"
	"strconv"
)

type StartQuizRoute struct {
}

type StartQuizPayload struct {
}

type StartQuizResponse struct {
	Success bool `json:"success"`
}

func NewStartQuizRoute() *StartQuizRoute {
	return &StartQuizRoute{}
}

func (r *StartQuizRoute) Payload() interface{} {
	return &StartQuizPayload{}
}

func (r *StartQuizRoute) Do(ctx context.Context, payload interface{}) (*utils.ApiResponse, error) {
	token, ok := utils.GetTokenFromContext(ctx).(*models.Token)
	if !ok {
		return nil, errors.New("Could not get token from ctx")
	}

	vars, ok := utils.GetVarsFromContext(ctx).(map[string]string)
	if !ok {
		return nil, errors.New("Could not get vars from ctx")
	}

	quizVar, ok := vars["quiz"]
	if !ok || quizVar == "" {
		return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Missing quiz id in path")
	}
	tmpQuiz, err := strconv.ParseUint(quizVar, 10, 32)
	if err != nil {
		return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Quiz id should be int")
	}
	quizID := uint(tmpQuiz)

	admin, err := models.GetAdmin(token.UserId)
	if err != nil {
		return nil, err
	}

	quiz, err := models.GetQuiz(quizID)
	if err != nil {
		return nil, err
	}
	if quiz.AdminID != admin.ID {
		return nil, utils.NewApiError(http.StatusForbidden, "You have no access for this quiz")
	}

	if err = quiz.Start(); err != nil {
		return nil, err
	}

	marshaledResp, err := json.Marshal(StartQuizResponse{Success: true})
	if err != nil {
		return nil, err
	}

	return &utils.ApiResponse{
		StatusCode: http.StatusOK,
		Response:   marshaledResp,
	}, nil
}
