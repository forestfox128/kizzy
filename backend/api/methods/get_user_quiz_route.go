package methods

import (
	"context"
	"encoding/json"
	"errors"
	"kizzy/api/objects"
	"kizzy/api/utils"
	"kizzy/models"
	"net/http"
)

type GetUserQuizRoute struct {
}

type GetUserQuizPayload struct {
}

type GetUserQuizResponse struct {
	objects.QuizObject
}

func NewGetUserQuizRoute() *GetUserQuizRoute {
	return &GetUserQuizRoute{}
}

func (r *GetUserQuizRoute) Payload() interface{} {
	return &GetUserQuizPayload{}
}

func (r *GetUserQuizRoute) Do(ctx context.Context, payload interface{}) (*utils.ApiResponse, error) {
	token, ok := utils.GetTokenFromContext(ctx).(*models.Token)
	if !ok {
		return nil, errors.New("Could not get token from ctx")
	}

	user, err := models.GetUser(token.UserId)
	if err != nil {
		return nil, err
	}
	if !user.Active {
		return nil, utils.NewApiError(http.StatusConflict, "User is not active")
	}

	quiz, err := models.GetQuiz(user.QuizID)
	if err != nil {
		return nil, err
	}

	if !quiz.Started {
		return nil, utils.NewApiError(http.StatusNotFound, "Quiz not found")
	}

	marshaledResp, err := json.Marshal(GetUserQuizResponse{
		QuizObject: *quiz.GetObjectFromModel(true),
	})
	if err != nil {
		return nil, err
	}

	return &utils.ApiResponse{
		StatusCode: http.StatusCreated,
		Response:   marshaledResp,
	}, nil
}
