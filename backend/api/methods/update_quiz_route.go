package methods

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"kizzy/api/objects"
	"kizzy/api/utils"
	"kizzy/models"
	"log"
	"net/http"
	"strconv"
)

type UpdateQuizRoute struct {
}

type UpdateQuizPayload struct {
	objects.QuizObject
}

type UpdateQuizResponse struct {
	objects.QuizObject
}

func NewUpdateQuizRoute() *UpdateQuizRoute {
	return &UpdateQuizRoute{}
}

func (r *UpdateQuizRoute) Payload() interface{} {
	return &UpdateQuizPayload{}
}

func (r *UpdateQuizRoute) validate(payload interface{}) (*UpdateQuizPayload, error) {
	reqPayload, ok := payload.(*UpdateQuizPayload)
	if !ok {
		return nil, errors.New("Invalid type assertion")
	}

	for _, formField := range reqPayload.RegistrationFields {
		if formField.Name == "" {
			return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Registration field should has non empty name")
		}
		if !objects.IsValidRegistrationField(formField.FieldType) {
			return nil, utils.NewApiError(http.StatusUnprocessableEntity, fmt.Sprintf("Wrong type of registration field: %s", formField.FieldType))
		}
	}

	for _, question := range reqPayload.Questions {
		if question.Question == "" {
			return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Question in question could not be empty")
		}
		isAnyCorrect := false
		for _, answer := range question.Answers {
			if answer.Value == "" {
				return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Value in answer is required")
			}
			if answer.IsCorrect {
				if isAnyCorrect {
					return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Only one answer for question has to be correct")
				}
				isAnyCorrect = true
			}
		}
		if !isAnyCorrect {
			return nil, utils.NewApiError(http.StatusUnprocessableEntity, "One answer for question has to be correct")
		}
	}
	return reqPayload, nil
}

func (r *UpdateQuizRoute) formatFormFields(registrationFormFields objects.RegistrationFields) []*models.RegistrationFormField {
	modelRegistrationFields := make([]*models.RegistrationFormField, 0, len(registrationFormFields))
	for _, formField := range registrationFormFields {
		modelRegistrationFields = append(modelRegistrationFields, &models.RegistrationFormField{
			Type:     formField.FieldType,
			Name:     formField.Name,
			Required: formField.Required,
		})
	}
	return modelRegistrationFields
}

func (r *UpdateQuizRoute) formatAnswers(questionAnswers objects.AnswersObject) []*models.Answer {
	modelQuestionAnswers := make([]*models.Answer, 0, len(questionAnswers))
	for _, answer := range questionAnswers {
		modelAnswer := &models.Answer{
			Value:     answer.Value,
			IsCorrect: answer.IsCorrect,
		}
		modelQuestionAnswers = append(modelQuestionAnswers, modelAnswer)
	}

	return modelQuestionAnswers
}

func (r *UpdateQuizRoute) formatQuestions(questions objects.QuestionsObject) []*models.Question {
	modelQuestions := make([]*models.Question, 0, len(questions))
	for _, question := range questions {
		modelQuestion := &models.Question{
			Value:        question.Question,
			IsMulti:      question.IsMulti,
			QuestionTime: question.QuestionTime,
			Answers:      r.formatAnswers(question.Answers),
		}
		log.Printf("%+v\n", modelQuestion)
		modelQuestions = append(modelQuestions, modelQuestion)
	}
	return modelQuestions
}

func (r *UpdateQuizRoute) Do(ctx context.Context, payload interface{}) (*utils.ApiResponse, error) {
	reqPayload, err := r.validate(payload)
	if err != nil {
		return nil, err
	}

	token, ok := utils.GetTokenFromContext(ctx).(*models.Token)
	if !ok {
		return nil, errors.New("Could not get token from ctx")
	}

	vars, ok := utils.GetVarsFromContext(ctx).(map[string]string)
	if !ok {
		return nil, errors.New("Could not get vars from ctx")
	}

	quizVar, ok := vars["quiz"]
	if !ok || quizVar == "" {
		return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Missing quiz id in path")
	}
	tmpQuiz, err := strconv.ParseUint(quizVar, 10, 32)
	if err != nil {
		return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Quiz id should be int")
	}
	quizID := uint(tmpQuiz)

	admin, err := models.GetAdmin(token.UserId)
	if err != nil {
		return nil, err
	}

	quiz, err := models.GetQuiz(quizID)
	if err != nil {
		return nil, err
	}
	if quiz.AdminID != admin.ID {
		return nil, utils.NewApiError(http.StatusForbidden, "You have no access for this quiz")
	}
	if quiz.Started || quiz.Finished {
		return nil, utils.NewApiError(http.StatusForbidden, "You can't update quiz that is started or finished")
	}

	modelRegistrationFormFields := r.formatFormFields(reqPayload.RegistrationFields)
	modelQuestions := r.formatQuestions(reqPayload.Questions)

	if reqPayload.Name != "" {
		quiz.Name = reqPayload.Name
	}

	if len(modelRegistrationFormFields) != 0 {
		quiz.DeleteFormFields()
		quiz.RegistrationFormFields = modelRegistrationFormFields
	}

	if len(modelQuestions) != 0 {
		quiz.DeleteQuestions()
		quiz.Questions = modelQuestions
	}

	if err := quiz.Update(); err != nil {
		return nil, err
	}

	response := &UpdateQuizResponse{QuizObject: *quiz.GetObjectFromModel(false)}

	marshaledResp, err := json.Marshal(response)
	if err != nil {
		return nil, err
	}

	return &utils.ApiResponse{
		StatusCode: http.StatusOK,
		Response:   marshaledResp,
	}, nil
}
