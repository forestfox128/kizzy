package methods

import (
	"kizzy/api/middlewares"
	"kizzy/api/utils"
	"kizzy/config"
	"kizzy/dispatcher"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
	Middlewares []func(next http.Handler) http.Handler
}

var baseMiddlewares []func(next http.Handler) http.Handler = []func(next http.Handler) http.Handler{
	func(next http.Handler) http.Handler {
		return handlers.CombinedLoggingHandler(os.Stderr, next)
	},
	middlewares.NewCORSMiddleware(),
	middlewares.NewGeoMiddleware(),
}

type Routes []Route

func GetRoutes(c *config.Config, dis *dispatcher.Dispatcher, rc *dispatcher.RedisConsumer) Routes {
	return Routes{
		Route{
			Name:    "Test Route",
			Method:  "GET",
			Pattern: "/ping", //tested
			HandlerFunc: utils.HandlerWrapper(
				NewTestRoute(),
			),
			Middlewares: append(baseMiddlewares, []func(next http.Handler) http.Handler{
				middlewares.NewAuthMiddleware(c, "admin"),
			}...),
		},
		Route{
			Name:    "Register Admin Route",
			Method:  "POST",
			Pattern: "/api/admin", //tested
			HandlerFunc: utils.HandlerWrapper(
				NewRegisterAdminRoute(),
			),
			Middlewares: baseMiddlewares,
		},
		Route{
			Name:    "Login Admin Route",
			Method:  "POST",
			Pattern: "/api/auth/administrator", //tested
			HandlerFunc: utils.HandlerWrapper(
				NewLoginAdminRoute(),
			),
			Middlewares: baseMiddlewares,
		},
		Route{
			Name:    "Auth User Route",
			Method:  "POST",
			Pattern: "/api/auth/user",
			HandlerFunc: utils.HandlerWrapper(
				NewAuthUserRoute(),
			),
			Middlewares: baseMiddlewares,
		},
		Route{
			Name:    "Activate User Route",
			Method:  "POST",
			Pattern: "/api/auth/user/activate",
			HandlerFunc: utils.HandlerWrapper(
				NewActivateUserRoute(),
			),
			Middlewares: append(baseMiddlewares, []func(next http.Handler) http.Handler{
				middlewares.NewAuthMiddleware(c, "user"),
			}...),
		},
		Route{
			Name:    "List Quiz Route",
			Method:  "GET",
			Pattern: "/api/admin/quiz", //tested
			HandlerFunc: utils.HandlerWrapper(
				NewListQuizRoute(),
			),
			Middlewares: append(baseMiddlewares, []func(next http.Handler) http.Handler{
				middlewares.NewAuthMiddleware(c, "admin"),
			}...),
		},
		Route{
			Name:    "Create Quiz Route",
			Method:  "POST",
			Pattern: "/api/admin/quiz", //tested
			HandlerFunc: utils.HandlerWrapper(
				NewCreateQuizRoute(),
			),
			Middlewares: append(baseMiddlewares, []func(next http.Handler) http.Handler{
				middlewares.NewAuthMiddleware(c, "admin"),
			}...),
		},
		Route{
			Name:    "Get Quiz Route",
			Method:  "GET",
			Pattern: "/api/admin/quiz/{quiz}", //tested
			HandlerFunc: utils.HandlerWrapper(
				NewGetQuizRoute(),
			),
			Middlewares: append(baseMiddlewares, []func(next http.Handler) http.Handler{
				middlewares.NewAuthMiddleware(c, "admin"),
			}...),
		},
		Route{
			Name:    "Delete Quiz Route",
			Method:  "DELETE",
			Pattern: "/api/admin/quiz/{quiz}", //tested
			HandlerFunc: utils.HandlerWrapper(
				NewDeleteQuizRoute(),
			),
			Middlewares: append(baseMiddlewares, []func(next http.Handler) http.Handler{
				middlewares.NewAuthMiddleware(c, "admin"),
			}...),
		},
		Route{
			Name:    "Update Quiz Route",
			Method:  "PUT",
			Pattern: "/api/admin/quiz/{quiz}",
			HandlerFunc: utils.HandlerWrapper(
				NewUpdateQuizRoute(),
			),
			Middlewares: append(baseMiddlewares, []func(next http.Handler) http.Handler{
				middlewares.NewAuthMiddleware(c, "admin"),
			}...),
		},
		Route{
			Name:    "Start Quiz Route",
			Method:  "PUT",
			Pattern: "/api/admin/quiz/{quiz}/start",
			HandlerFunc: utils.HandlerWrapper(
				NewStartQuizRoute(),
			),
			Middlewares: append(baseMiddlewares, []func(next http.Handler) http.Handler{
				middlewares.NewAuthMiddleware(c, "admin"),
			}...),
		},
		Route{
			Name:    "Finish Quiz Route",
			Method:  "PUT",
			Pattern: "/api/admin/quiz/{quiz}/finish",
			HandlerFunc: utils.HandlerWrapper(
				NewFinishQuizRoute(),
			),
			Middlewares: append(baseMiddlewares, []func(next http.Handler) http.Handler{
				middlewares.NewAuthMiddleware(c, "admin"),
			}...),
		},
		Route{
			Name:    "Get User Quiz Route",
			Method:  "GET",
			Pattern: "/api/user/quiz",
			HandlerFunc: utils.HandlerWrapper(
				NewGetUserQuizRoute(),
			),
			Middlewares: append(baseMiddlewares, []func(next http.Handler) http.Handler{
				middlewares.NewAuthMiddleware(c, "user"),
			}...),
		},
		Route{
			Name:    "Get User Quiz Results Route",
			Method:  "GET",
			Pattern: "/api/user/results",
			HandlerFunc: utils.HandlerWrapper(
				NewGetUserQuizResultsRoute(),
			),
			Middlewares: append(baseMiddlewares, []func(next http.Handler) http.Handler{
				middlewares.NewAuthMiddleware(c, "user"),
			}...),
		},
		Route{
			Name:    "Update User Answer Route",
			Method:  "PUT",
			Pattern: "/api/user/quiz/{quiz}/answer",
			HandlerFunc: utils.HandlerWrapper(
				NewUpdateQuizAnswerRoute(),
			),
			Middlewares: append(baseMiddlewares, []func(next http.Handler) http.Handler{
				middlewares.NewAuthMiddleware(c, "user"),
			}...),
		},
		Route{
			Name:    "Update User Answer Route",
			Method:  "PUT",
			Pattern: "/api/user/quiz/{quiz}/finish",
			HandlerFunc: utils.HandlerWrapper(
				NewFinishUserQuizRoute(),
			),
			Middlewares: append(baseMiddlewares, []func(next http.Handler) http.Handler{
				middlewares.NewAuthMiddleware(c, "user"),
			}...),
		},
		Route{
			Name:    "Get Quiz Report Route",
			Method:  "GET",
			Pattern: "/api/admin/quiz/{quiz}/report",
			HandlerFunc: utils.HandlerWrapper(
				NewGetQuizReportRoute(),
			),
			Middlewares: append(baseMiddlewares, []func(next http.Handler) http.Handler{
				middlewares.NewAuthMiddleware(c, "admin"),
			}...),
		},
		Route{
			Name:    "Get User Report Route",
			Method:  "GET",
			Pattern: "/api/admin/quiz/{quiz}/report/{user}",
			HandlerFunc: utils.HandlerWrapper(
				NewGetUserReportRoute(),
			),
			Middlewares: append(baseMiddlewares, []func(next http.Handler) http.Handler{
				middlewares.NewAuthMiddleware(c, "admin"),
			}...),
		},
		Route{
			Name:    "WebSocket endpoint",
			Method:  "GET",
			Pattern: "/ws",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				dispatcher.ServeWs(dis, w, r)
			},
			Middlewares: baseMiddlewares,
		},
		Route{
			Name:    "Add validation event",
			Method:  "POST",
			Pattern: "/api/admin/quiz/{quiz}/validate",
			HandlerFunc: utils.HandlerWrapper(
				NewSendQuizEventRoute(rc),
			),
			Middlewares: append(baseMiddlewares, []func(next http.Handler) http.Handler{
				middlewares.NewAuthMiddleware(c, "admin"),
			}...),
		},
		Route{
			Name:    "Update User Answer Route",
			Method:  "PUT",
			Pattern: "/api/user/quiz/{quiz}/validate",
			HandlerFunc: utils.HandlerWrapper(
				NewAnswerValidationEventRoute(),
			),
			Middlewares: append(baseMiddlewares, []func(next http.Handler) http.Handler{
				middlewares.NewAuthMiddleware(c, "user"),
			}...),
		},
	}
}
