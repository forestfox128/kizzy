package methods

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"kizzy/api/objects"
	"kizzy/api/utils"
	"kizzy/models"
	"net/http"
)

type CreateQuizRoute struct {
}

type CreateQuizPayload struct {
	objects.QuizObject
}

type CreateQuizResponse struct {
	objects.QuizObject
}

func NewCreateQuizRoute() *CreateQuizRoute {
	return &CreateQuizRoute{}
}

func (r *CreateQuizRoute) Payload() interface{} {
	return &CreateQuizPayload{}
}

func (r *CreateQuizRoute) validate(payload interface{}) (*CreateQuizPayload, error) {
	reqPayload, ok := payload.(*CreateQuizPayload)
	if !ok {
		return nil, errors.New("Invalid type assertion")
	}
	if reqPayload.Name == "" {
		return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Name is required")
	}
	if len(reqPayload.Questions) == 0 {
		return nil, utils.NewApiError(http.StatusUnprocessableEntity, "At least one question is required")
	}

	for _, formField := range reqPayload.RegistrationFields {
		if formField.Name == "" {
			return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Registration field should has non empty name")
		}
		if !objects.IsValidRegistrationField(formField.FieldType) {
			return nil, utils.NewApiError(http.StatusUnprocessableEntity, fmt.Sprintf("Wrong type of registration field: %s", formField.FieldType))
		}
	}

	for _, question := range reqPayload.Questions {
		if question.Question == "" {
			return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Question in question could not be empty")
		}
		if len(question.Answers) == 0 {
			return nil, utils.NewApiError(http.StatusUnprocessableEntity, "You have to provide answers for question")
		}
		if question.IsMulti {
			continue
		}
		isAnyCorrect := false
		for _, answer := range question.Answers {
			if answer.Value == "" {
				return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Value in answer is required")
			}
			if answer.IsCorrect {
				if isAnyCorrect {
					return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Only one answer for question has to be correct")
				}
				isAnyCorrect = true
			}
		}
		if !isAnyCorrect {
			return nil, utils.NewApiError(http.StatusUnprocessableEntity, "One answer should be correct")
		}
	}
	return reqPayload, nil
}

func (r *CreateQuizRoute) formatFormFields(registrationFormFields objects.RegistrationFields) []*models.RegistrationFormField {
	modelRegistrationFields := make([]*models.RegistrationFormField, 0, len(registrationFormFields))
	for _, formField := range registrationFormFields {
		modelRegistrationFields = append(modelRegistrationFields, &models.RegistrationFormField{
			Type:     formField.FieldType,
			Name:     formField.Name,
			Required: formField.Required,
			Values:   formField.PossibleValues,
		})
	}
	return modelRegistrationFields
}

func (r *CreateQuizRoute) formatAnswers(questionAnswers objects.AnswersObject) []*models.Answer {
	modelQuestionAnswers := make([]*models.Answer, 0, len(questionAnswers))
	for _, answer := range questionAnswers {
		modelQuestionAnswers = append(modelQuestionAnswers, &models.Answer{
			Value:     answer.Value,
			IsCorrect: answer.IsCorrect,
		})
	}

	return modelQuestionAnswers
}

func (r *CreateQuizRoute) formatQuestions(questions objects.QuestionsObject) []*models.Question {
	modelQuestions := make([]*models.Question, 0, len(questions))
	for _, question := range questions {
		modelQuestions = append(modelQuestions, &models.Question{
			Value:        question.Question,
			IsMulti:      question.IsMulti,
			QuestionTime: question.QuestionTime,
			Answers:      r.formatAnswers(question.Answers),
		})
	}
	return modelQuestions
}

func (r *CreateQuizRoute) Do(ctx context.Context, payload interface{}) (*utils.ApiResponse, error) {
	reqPayload, err := r.validate(payload)
	if err != nil {
		return nil, err
	}

	token, ok := utils.GetTokenFromContext(ctx).(*models.Token)
	if !ok {
		return nil, errors.New("Could not get token from ctx")
	}

	admin, err := models.GetAdmin(token.UserId)
	if err != nil {
		return nil, err
	}

	modelRegistrationFormFields := r.formatFormFields(reqPayload.RegistrationFields)
	modelQuestions := r.formatQuestions(reqPayload.Questions)

	quiz, err := models.CreateQuiz(&reqPayload.QuizObject, modelRegistrationFormFields, modelQuestions, admin.ID)
	if err != nil {
		return nil, err
	}

	response := CreateQuizResponse{QuizObject: *quiz.GetObjectFromModel(false)}

	marshaledResp, err := json.Marshal(response)
	if err != nil {
		return nil, err
	}

	return &utils.ApiResponse{
		StatusCode: http.StatusOK,
		Response:   marshaledResp,
	}, nil
}
