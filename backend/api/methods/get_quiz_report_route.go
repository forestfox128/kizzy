package methods

import (
	"context"
	"encoding/json"
	"errors"
	"kizzy/api/objects"
	"kizzy/api/utils"
	"kizzy/models"
	"net/http"
	"strconv"
)

type GetQuizReportRoute struct {
}

type GetQuizReportPayload struct {
}

type GetQuizReportResponse struct {
	objects.QuizObject
	Users []*objects.UserObject `json:"users"`
}

func NewGetQuizReportRoute() *GetQuizReportRoute {
	return &GetQuizReportRoute{}
}

func (r *GetQuizReportRoute) Payload() interface{} {
	return &GetQuizReportPayload{}
}

func (r *GetQuizReportRoute) validateQuizID(ctx context.Context) (uint, error) {
	vars, ok := utils.GetVarsFromContext(ctx).(map[string]string)
	if !ok {
		return 0, errors.New("Could not get vars from ctx")
	}

	quizVar, ok := vars["quiz"]
	if !ok || quizVar == "" {
		return 0, utils.NewApiError(http.StatusUnprocessableEntity, "Missing quiz id in path")
	}
	tmpQuiz, err := strconv.ParseUint(quizVar, 10, 32)
	if err != nil {
		return 0, utils.NewApiError(http.StatusUnprocessableEntity, "Quiz id should be int")
	}
	return uint(tmpQuiz), nil
}

func (r *GetQuizReportRoute) generateReportResponse(quiz *objects.QuizObject, users []*models.User) *GetQuizReportResponse {
	answersMapCount := make(map[uint]map[uint]int)
	usersObjects := make([]*objects.UserObject, 0, len(users))
	for _, user := range users {
		usersObjects = append(usersObjects, user.GenerateUserObject())
		for _, userAnswer := range user.UserAnswers {
			if _, exists := answersMapCount[userAnswer.QuestionID]; !exists {
				answersMapCount[userAnswer.QuestionID] = make(map[uint]int)
			}
			if _, exists := answersMapCount[userAnswer.QuestionID][userAnswer.AnswerID]; !exists {
				answersMapCount[userAnswer.QuestionID][userAnswer.AnswerID] = 0
			}
			answersMapCount[userAnswer.QuestionID][userAnswer.AnswerID]++
		}
	}
	for _, question := range quiz.Questions {
		for _, answer := range question.Answers {
			timesAnswered := 0
			if _, exists := answersMapCount[question.ID]; exists {
				if times, exists := answersMapCount[question.ID][answer.ID]; exists {
					timesAnswered = times
				}
			}
			answer.Anwsered = timesAnswered
		}
	}

	return &GetQuizReportResponse{
		QuizObject: *quiz,
		Users:      usersObjects,
	}
}

func (r *GetQuizReportRoute) Do(ctx context.Context, payload interface{}) (*utils.ApiResponse, error) {
	token, ok := utils.GetTokenFromContext(ctx).(*models.Token)
	if !ok {
		return nil, errors.New("Could not get token from ctx")
	}

	quizID, err := r.validateQuizID(ctx)
	if err != nil {
		return nil, err
	}

	admin, err := models.GetAdmin(token.UserId)
	if err != nil {
		return nil, err
	}

	quiz, err := models.GetQuiz(quizID)
	if err != nil {
		return nil, err
	}
	if quiz.AdminID != admin.ID {
		return nil, utils.NewApiError(http.StatusForbidden, "You have no access for this quiz")
	}

	users, err := models.GetUsersByQuizID(quiz.ID)
	if err != nil {
		return nil, err
	}

	marshaledResp, err := json.Marshal(r.generateReportResponse(quiz.GetObjectFromModel(false), users))
	if err != nil {
		return nil, err
	}

	return &utils.ApiResponse{
		StatusCode: http.StatusOK,
		Response:   marshaledResp,
	}, nil
}
