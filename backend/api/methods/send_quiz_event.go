package methods

import (
	"context"
	"encoding/json"
	"errors"
	"kizzy/api/objects"
	"kizzy/api/utils"
	"kizzy/dispatcher"
	"kizzy/models"
	"net/http"
	"strconv"
)

type SendQuizEventRoute struct {
	redisConsumer *dispatcher.RedisConsumer
}

type SendQuizEventPayload struct {
	objects.ValidationEvent
}

type SendQuizEventResponse struct {
	Success bool `json:"success"`
}

func NewSendQuizEventRoute(rc *dispatcher.RedisConsumer) *SendQuizEventRoute {
	return &SendQuizEventRoute{
		redisConsumer: rc,
	}
}

func (r *SendQuizEventRoute) Payload() interface{} {
	return &SendQuizEventPayload{}
}

func (r *SendQuizEventRoute) validate(payload interface{}) (*SendQuizEventPayload, error) {
	reqPayload, ok := payload.(*SendQuizEventPayload)
	if !ok {
		return nil, errors.New("Invalid type assertion")
	}
	if reqPayload.EventValues == nil || len(reqPayload.EventValues) == 0 {
		return nil, utils.NewApiError(http.StatusUnprocessableEntity, "You need to pass at least on `validation value`")
	}
	if reqPayload.CorrectValue == "" {
		return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Correct value is required")
	}

	for _, value := range reqPayload.EventValues {
		if value == "" {
			return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Value in `values` can't be empty")
		}
		if value == reqPayload.CorrectValue {
			return reqPayload, nil
		}
	}
	return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Correct value should be among `validation_values`")
}

func (r *SendQuizEventRoute) Do(ctx context.Context, payload interface{}) (*utils.ApiResponse, error) {
	token, ok := utils.GetTokenFromContext(ctx).(*models.Token)
	if !ok {
		return nil, errors.New("Could not get token from ctx")
	}

	vars, ok := utils.GetVarsFromContext(ctx).(map[string]string)
	if !ok {
		return nil, errors.New("Could not get vars from ctx")
	}

	quizVar, ok := vars["quiz"]
	if !ok || quizVar == "" {
		return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Missing quiz id in path")
	}
	tmpQuiz, err := strconv.ParseUint(quizVar, 10, 32)
	if err != nil {
		return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Quiz id should be int")
	}

	reqPayload, err := r.validate(payload)
	if err != nil {
		return nil, err
	}

	quizID := uint(tmpQuiz)

	admin, err := models.GetAdmin(token.UserId)
	if err != nil {
		return nil, err
	}

	quiz, err := models.GetQuiz(quizID)
	if err != nil {
		return nil, err
	}
	if quiz.AdminID != admin.ID {
		return nil, utils.NewApiError(http.StatusForbidden, "You have no access for this quiz")
	}

	eventDB, err := quiz.AddEvent(reqPayload.ValidationEvent)
	if err != nil {
		return nil, err
	}

	reqPayload.ValidationEvent.CorrectValue = ""
	r.redisConsumer.Publish("quiz", quiz.ID, eventDB.ID, reqPayload.ValidationEvent)

	marshaledResp, err := json.Marshal(SendQuizEventResponse{Success: true})
	if err != nil {
		return nil, err
	}

	return &utils.ApiResponse{
		StatusCode: http.StatusOK,
		Response:   marshaledResp,
	}, nil
}
