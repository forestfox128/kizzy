package methods

import (
	"context"
	"encoding/json"
	"errors"
	"kizzy/api/utils"
	"kizzy/models"
	"net/http"
	"strconv"
)

type FinishUserQuizRoute struct {
}

type FinishUserQuizPayload struct {
}

type FinishUserQuizResponse struct {
	Questions      int `json:"questions"`
	CorrectAnswers int `json:"correct_answers"`
	MissingAnswers int `json:"missing_answers`
	OverTime       int `json:"over_time"`
}

func NewFinishUserQuizRoute() *FinishUserQuizRoute {
	return &FinishUserQuizRoute{}
}

func (r *FinishUserQuizRoute) Payload() interface{} {
	return &FinishUserQuizPayload{}
}

func (r *FinishUserQuizRoute) validate(vars map[string]string) (uint, error) {
	quizVar, exists := vars["quiz"]
	if !exists {
		return 0, utils.NewApiError(http.StatusBadRequest, "Missing `quiz_id` in path")
	}
	quizID, err := strconv.ParseUint(quizVar, 10, 32)
	if err != nil {
		return 0, utils.NewApiError(http.StatusUnprocessableEntity, "Quiz id should be int")
	}
	return uint(quizID), nil
}

func (r *FinishUserQuizRoute) calculateQuestions(answers []*models.UserAnswer, questions []*models.Question) (int, int, int, int) {
	questionsTotalNumber := len(questions)
	missedQuestions := 0
	correctAnswers := 0
	overTimeQuestions := 0
	userAnswersMapped := make(map[uint]*models.UserAnswer)

	for _, userAnswer := range answers {
		userAnswersMapped[userAnswer.QuestionID] = userAnswer
	}

	for _, question := range questions {

		if userAnswer, exists := userAnswersMapped[question.ID]; exists {
			var qt int
			if question.QuestionTime == 0 {
				qt = 60
			} else {
				qt = int(question.QuestionTime)
			}
			if qt < int(userAnswer.TimeTaken) {
				overTimeQuestions++
			}
			if question.IsMulti {
				answeredProperly := 0
				answeredImproperly := 0
				properAnswers := 0
				for _, answerModel := range question.Answers {
					found := false
					if exists {
						for _, userAnswerMultiID := range userAnswer.GetAnswersIDs() {
							if userAnswerMultiID == userAnswer.AnswerID {
								found = true
							}
						}
					}
					if found {
						if answerModel.IsCorrect {
							answeredProperly++
						}
					} else {
						if answerModel.IsCorrect {
							answeredImproperly++
						} else if exists {
							answeredProperly++
						} else {
							answeredImproperly++
						}
					}

				}
				if answeredImproperly == 0 && answeredProperly == properAnswers {
					correctAnswers++
				}
			} else {
				for _, answer := range question.Answers {
					if answer.IsCorrect && answer.ID == userAnswer.AnswerID {
						correctAnswers++
						break
					}
				}
			}
		} else {
			missedQuestions++
		}
	}
	return correctAnswers, missedQuestions, questionsTotalNumber, overTimeQuestions
}

func (r *FinishUserQuizRoute) Do(ctx context.Context, payload interface{}) (*utils.ApiResponse, error) {
	token, ok := utils.GetTokenFromContext(ctx).(*models.Token)
	if !ok {
		return nil, errors.New("Could not get token from ctx")
	}
	vars, ok := utils.GetVarsFromContext(ctx).(map[string]string)
	if !ok {
		return nil, errors.New("Could not get vars from ctx")
	}
	quizID, err := r.validate(vars)
	if err != nil {
		return nil, err
	}

	user, err := models.GetUser(token.UserId)
	if err != nil {
		return nil, err
	}
	if !user.Active {
		return nil, utils.NewApiError(http.StatusConflict, "User has to be activated first")
	}
	if user.HasFinishedQuiz {
		return nil, utils.NewApiError(http.StatusConflict, "User is already finished quiz")
	}

	quiz, err := models.GetQuiz(quizID)
	if err != nil {
		return nil, err
	}

	if quiz.ID != user.QuizID {
		return nil, utils.NewApiError(http.StatusForbidden, "You have no access for this quiz")
	}

	if err := user.MarkQuizAsFinished(); err != nil {
		return nil, err
	}

	correct, missing, all, overTime := r.calculateQuestions(user.UserAnswers, quiz.Questions)

	marshaledResp, err := json.Marshal(FinishUserQuizResponse{
		CorrectAnswers: correct,
		MissingAnswers: missing,
		Questions:      all,
		OverTime:       overTime,
	})
	if err != nil {
		return nil, err
	}

	return &utils.ApiResponse{
		StatusCode: http.StatusCreated,
		Response:   marshaledResp,
	}, nil
}
