package methods

import (
	"context"
	"encoding/json"
	"errors"
	"kizzy/api/objects"
	"kizzy/api/utils"
	"kizzy/models"
	"net/http"
)

type LoginAdminRoute struct {
}

type LoginAdminPayload struct {
	Login    string `json:"email"`
	Password string `json:"password"`
}

type LoginAdminResponse struct {
	Token string              `json:"token"`
	Admin objects.AdminObject `json:"admin"`
}

func NewLoginAdminRoute() *LoginAdminRoute {
	return &LoginAdminRoute{}
}

func (r *LoginAdminRoute) Payload() interface{} {
	return &LoginAdminPayload{}
}

func (r *LoginAdminRoute) validate(payload interface{}) (*LoginAdminPayload, error) {
	reqPayload, ok := payload.(*LoginAdminPayload)
	if !ok {
		return nil, errors.New("Invalid type assertion")
	}
	if reqPayload.Login == "" {
		return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Email is required")
	}
	if reqPayload.Password == "" {
		return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Password is required")
	}
	return reqPayload, nil
}

func (r *LoginAdminRoute) Do(ctx context.Context, payload interface{}) (*utils.ApiResponse, error) {
	reqPayload, err := r.validate(payload)
	if err != nil {
		return nil, err
	}
	admin, err := models.LoginAdmin(reqPayload.Login, reqPayload.Password)
	if err != nil {
		return nil, err
	}
	adminObject := objects.AdminObject{
		Name:  admin.Name,
		Email: admin.Email,
	}

	marshaledResp, err := json.Marshal(LoginAdminResponse{
		Admin: adminObject,
		Token: admin.Token,
	})
	if err != nil {
		return nil, err
	}

	return &utils.ApiResponse{
		StatusCode: http.StatusOK,
		Response:   marshaledResp,
	}, nil
}
