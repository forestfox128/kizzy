package methods

import (
	"context"
	"encoding/json"
	"errors"
	"kizzy/api/objects"
	"kizzy/api/utils"
	"kizzy/models"
	"net/http"
	"strings"
)

type RegisterAdminRoute struct {
}

type RegisterAdminPayload struct {
	objects.AdminObject
}

type RegisterAdminResponse struct {
	Token string              `json:"token"`
	Admin objects.AdminObject `json:"admin"`
}

func NewRegisterAdminRoute() *RegisterAdminRoute {
	return &RegisterAdminRoute{}
}

func (r *RegisterAdminRoute) Payload() interface{} {
	return &RegisterAdminPayload{}
}

func (r *RegisterAdminRoute) validate(payload interface{}) (*RegisterAdminPayload, error) {
	reqPayload, ok := payload.(*RegisterAdminPayload)
	if !ok {
		return nil, errors.New("Invalid type assertion")
	}
	if reqPayload.Name == "" {
		return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Name is required")
	}
	if reqPayload.Email == "" || !strings.Contains(reqPayload.Email, "@") {
		return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Correct email is required")
	}
	if reqPayload.Password == "" || len(reqPayload.Password) < 6 {
		return nil, utils.NewApiError(http.StatusUnprocessableEntity, "Password is required and should have at least 6 characters")
	}
	return reqPayload, nil
}

func (r *RegisterAdminRoute) Do(ctx context.Context, payload interface{}) (*utils.ApiResponse, error) {
	reqPayload, err := r.validate(payload)
	if err != nil {
		return nil, err
	}
	newAdmin, err := models.CreateAdmin(&reqPayload.AdminObject)
	if err != nil {
		return nil, err
	}

	adminObject := reqPayload.AdminObject
	adminObject.Password = ""

	marshaledResp, err := json.Marshal(RegisterAdminResponse{
		Admin: adminObject,
		Token: newAdmin.Token,
	})
	if err != nil {
		return nil, err
	}

	return &utils.ApiResponse{
		StatusCode: http.StatusCreated,
		Response:   marshaledResp,
	}, nil
}
