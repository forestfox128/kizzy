package middlewares

import (
	"kizzy/api/utils"
	geolocate "kizzy/geoloc"
	"log"
	"net"
	"net/http"
)

func NewGeoMiddleware() func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ip, _, _ := net.SplitHostPort(r.RemoteAddr)
			log.Println(ip)
			userIP := net.ParseIP(ip)
			userCity := geolocate.GetGeoForIP(userIP)
			r = r.WithContext(utils.CreateContextWithGeoLoc(r.Context(), userCity, userIP))
			next.ServeHTTP(w, r)
		})
	}
}
