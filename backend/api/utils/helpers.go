package utils

import (
	"context"
	"log"
	"net"

	"github.com/oschwald/geoip2-golang"
)

type tokenKey struct{}

func CreateContextWithToken(ctx context.Context, token interface{}) context.Context {
	return context.WithValue(ctx, tokenKey{}, token)
}

func GetTokenFromContext(ctx context.Context) interface{} {
	return ctx.Value(tokenKey{})
}

type varsKey struct{}

func CreateContextWithVars(ctx context.Context, vars interface{}) context.Context {
	return context.WithValue(ctx, varsKey{}, vars)
}

func GetVarsFromContext(ctx context.Context) interface{} {
	return ctx.Value(varsKey{})
}

type geoKey struct{}

type geoData struct {
	IP  net.IP
	Geo *geoip2.City
}

func CreateContextWithGeoLoc(ctx context.Context, cityIP *geoip2.City, ip net.IP) context.Context {
	data := geoData{
		IP:  ip,
		Geo: cityIP,
	}
	log.Println("User IP:", data)
	return context.WithValue(ctx, geoKey{}, data)
}

func GetGeoLocFromContext(ctx context.Context) (*geoip2.City, net.IP) {
	val := ctx.Value(geoKey{})

	geoVal, ok := val.(geoData)
	if !ok {
		log.Println("Failed interface assertion")
		log.Println(val)
		return nil, nil
	}
	return geoVal.Geo, geoVal.IP
}
