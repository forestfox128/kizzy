package objects

type AdminObject struct {
	ID       uint   `json:"-"`
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password,omitempty"`
}

type RegistrationField struct {
	FieldType      string      `json:"type,omitempty"`
	Name           string      `json:"name"`
	Value          interface{} `json:"value,omitempty"`
	Required       bool        `json:"required"`
	PossibleValues []string    `json:"values,omitempty"`
}

type RegistrationFields []*RegistrationField

type AnswerObject struct {
	ID        uint   `json:"id"`
	Value     string `json:"value"`
	IsCorrect bool   `json:"is_correct"`
	Anwsered  int    `json:"anwsered,omitempty"`
	AnswerIDs []int  `json:"answer_ids,omitempty"`
}

type AnswersObject []*AnswerObject

type QuestionObject struct {
	ID           uint          `json:"id"`
	Question     string        `json:"question"`
	Answers      AnswersObject `json:"answers"`
	IsMulti      bool          `json:"is_multi"`
	QuestionTime uint          `json:"time"`
}

type QuestionsObject []*QuestionObject

type QuizObject struct {
	ID                 uint               `json:"id"`
	Name               string             `json:"name"`
	AccessToken        string             `json:"access_token"`
	RegistrationFields RegistrationFields `json:"registration_fields"`
	Started            bool               `json:"started"`
	Finished           bool               `json:"finished"`
	Randomize          bool               `json:"randomize"`
	CreationDate       string             `json:"creation_date"`
	FinishDate         string             `json:"finish_date,omitempty"`
	Questions          QuestionsObject    `json:"questions"`
}

type UserGeoLocObject struct {
	City    string `json:"city"`
	Country string `json:"country"`
	IP      string `json:"ip"`
}

type UserAnswerObject struct {
	QuestionID uint   `json:"question_id"`
	AnswerID   uint   `json:"answer_id, omitempty"`
	TimeTaken  uint   `json:"time_taken"`
	AnswerIDs  []uint `json:"answer_ids,omitempty"`
}

type UserAnswersObject []*UserAnswerObject

type UserAnswerResultObject struct {
	Question         string   `json:"question"`
	Answer           string   `json:"answer,omitempty"`
	Answers          []string `json:"answers,omitempty"`
	CorrectAnswer    string   `json:"correct_answer,omitempty"`
	CorrectAnswers   []string `json:"correct_answers,omitempty"`
	IsAnswerCorrect  bool     `json:"is_correct"`
	IsAnswerOverTime bool     `json:"over_time"`
	AnswerTime       uint     `json:"-"`
	AnswerIDs        []uint   `json:"-"`
	IsMulti          bool     `json:"is_multi"`
	AnswerID         uint     `json:"-"`
}

type UserAnswersResultObject []*UserAnswerResultObject

type UserQuizResultObject struct {
	ID           uint                    `json:"id"`
	Name         string                  `json:"name"`
	CreationDate string                  `json:"creation_date"`
	FinishDate   string                  `json:"finish_date,omitempty"`
	Answers      UserAnswersResultObject `json:"answers"`
}

type UserEventAnswer struct {
	Answer    string `json:"answer"`
	IsCorrect bool   `json:"is_correct"`
}

type UserEventAnswers []*UserEventAnswer

type UserObject struct {
	ID                uint               `json:"id,omitempty"`
	Username          string             `json:"name"`
	UserFields        RegistrationFields `json:"fields"`
	GeoLoc            UserGeoLocObject   `json:"geolocation"`
	UserEventsAnswers UserEventAnswers   `json:"validation_events"`
}

type UserQuizReportObject struct {
	User        *UserObject             `json:"user"`
	UserAnswers UserAnswersResultObject `json:"answers"`
}

type ValidationEvent struct {
	EventValues  []string `json:"values"`
	CorrectValue string   `json:"correct,omitempty"`
}
