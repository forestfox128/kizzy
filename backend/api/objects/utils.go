package objects

func IsValidRegistrationField(fieldName string) bool {
	switch fieldName {
	case "text", "email", "number", "select", "dropdown":
		return true
	default:
		return false
	}
}
