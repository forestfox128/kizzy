module kizzy

go 1.12

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-delve/delve v1.3.2
	github.com/go-redis/redis v6.15.6+incompatible
	github.com/google/uuid v1.1.1 // indirect
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/schema v1.1.0
	github.com/gorilla/websocket v1.4.1
	github.com/jinzhu/gorm v1.9.11
	github.com/lib/pq v1.1.1
	github.com/oschwald/geoip2-golang v1.3.0
	github.com/oschwald/maxminddb-golang v1.5.0 // indirect
	github.com/speps/go-hashids v2.0.0+incompatible
	golang.org/x/crypto v0.0.0-20190325154230-a5d413f7728c
)
