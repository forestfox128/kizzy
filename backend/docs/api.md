# Kizzy API V1

```
DISCLAIMER: #TBI by method means To Be Implemented
Methods can vary and change during time, API is not stable or even finished ;) 
```

# Changes/changelog
- Added `geoloaction` field to user object:
  ```
  "geolocation": {
                "city": "",
                "country": "",
                "ip": "172.18.0.1"
            }
  ```
- Added `is_multi` to question object, als could be passed while creating or updating quiz
- Added `answers_ids` to answer object if user is answering multi-question field or field is retrieved by any user
- Added `time_taken` field for user answer, should be provided by front-end
- For reports there is also fields like: `correct_answers` (instead of `correct_answers`) and `answers` (instead of `answer`) used for multi questions
- New fields types for registration fields: `select` (value that user provides is matched against list of values), `dropdown` (user can select value from list). Values for this fields are passed by `values` field and can be only of type `string`.
- NOTE: Field `values` (Registration Form Field) for user request is not returned in case of `select` type.
- New field in user objects, in reports: `validation_events`

## Objects

### Registration field
Representation of filed used in registration form.

- `type` - string value, could be: `text`, `email`, `number`
- `name` - string value used to represent name of the field
- `required` - boolean value indicates if field is required
- `value` - given value of field

```
{
    "type": "text",
    "name": "text",
    "required": false,
    "value": "test"
}
```
### Answer
Object that represent answer for question

- `id` - id of given answer, integer
- `value` - string that represents value of
- `correct` - optional boolean field that indicates if answer was correct
- `is_correct` - optional value that indicates if answer is correct one

```
{
    "id": 1,
    "value": "Option 1",
    "correct": false,
    "is_correct": true
}
```

### Question
Object that represents the question
- `id` - question id, integer
- `question` - string that describes question
- `answers` - array of `answer` objects

```
{
    "id": 1,
    "question": "What will You choose?"
    "answers": [
        {
            "id": 1,
            "value": "Option 1",
        },
        {
            "id": 2,
            "value": "Option 2"
        },
        ...
    ]
}
```

### Quiz
Object that represents quiz
- `id` - id number of quiz
- `access_token` - `token` that allows users to login in app, semi-random string
- `name` - name of the quiz
- `registration_fields` - array of fields asked during registration
- `started` - boolean value that describes if quiz started
- `finished` - boolean value that describes if quiz finished
- `creation_date` - date of quiz creation yyyy-MM-dd'T'HH:mm:ss
- `finish_date` - date of quiz start in format yyyy-MM-dd'T'HH:mm:ss
- `questions` - array of `question` objects

```
{
    "id": 1,
    "name": "Test quiz",
    "access_token": "dasd2342gre"
    "registration_fields": [
        {
            "type": "text",
            "name": "gender",
            "required": true
        },
        {
            "type": "email",
            "name": "email",
            "required": true
        },
        {
            "type": "number",
            "name": "age",
            "required": false
        }
    ],
    "started": true,
    "finished": false,
    "creation_date": "2006-01-02T15:04:05",
    "finish_date": null,
    "questions": [
        {
            "id": 1,
            "question": "What will You choose?"
            "answers": [
                {
                    "id": 1,
                    "value": "Option 1",
                    "is_correct": true
                },
                {
                    "id": 2,
                    "value": "Option 2",
                    "is_correct": false
                },
                ...
            ]
        },
        {
            "id": 2,
            "question": "What will You choose again?"
            "answers": [
                {
                    "id": 1,
                    "value": "Option 21",
                    "is_correct": false
                },
                {
                    "id": 2,
                    "value": "Option 22",
                    "is_correct": true
                },
                ...
            ]
        },
        ...
    ]
}
```

### Admin
- `name` - account name
- `email` - account email

```
{
    "name": "Jamnik",
    "email": "jan@pawel.ii"
}
```

### User
- `id` - system generated id of user
- `name` - identification name of user
- `fields` - array of user `fields`


```
{
    "id": "sdasgyd3r64yrvf"
    "name": "Adrian",
    fields: [
        {
            "type": "text",
            "name": "gender",
            "value": "mucha"
        },
        {
            "type": "email",
            "name": "email",
            "value": "a.slucha@gmail.com"
        },
        {
            "type": "number",
            "name": "age",
            "value": 69
        }
    ]
}
```

## Auth

In order to use all endpoints, expect `auth` ones you will have to pass following header:
```
Authorization: Bearer <token>
```

### Administrator Auth
Login as administrator could be done via following endpoint:
```
POST /api/auth/administrator
----
{
    "email": "jan@pawel.ii",
    "password": "123parowki456"
}
```
Response:
```
{
    "token": "abcd-sefd-sads-123",
    "admin":
        {
            "name": "Jamnik",
            "email": "jan@pawel.ii"
        }
}
```

### User auth
To acquire user token, first you need to ask for it via:
```
POST /api/auth/user
----

{
    "quiz_access_token": "dsad232fds"
}
```
Response:
```
{
    "token": "abcd-das34-dsads-123",
    "fields": [
        {
            "type": "text",
            "name": "gender",
            "required": true
        },
        {
            "type": "email",
            "name": "email",
            "required": true
        },
        {
            "type": "number",
            "name": "age",
            "required": false
        }
    ]
}
```

Then you have to activate your token via:
```
POST /api/auth/user/activate
----
{
    "name": "Adrian",
    fields: [
        {
            "type": "text",
            "name": "gender",
            "value": "mucha"
        },
        {
            "type": "email",
            "name": "email",
            "value": "a.slucha@gmail.com"
        }
    ]
}
```
Response:
```
{
    "success": true
}
```

## Endpoints

### Admin endpoints
```
POST /api/admin
----
REQUEST:
{
    "name": "Jamnik",
    "email": "jan@pawel.ii",
    "password": "test123"
}
----
RESPONSE:
{
    "token": "abcd-sefd-sads-123",
    "admin":
        {
            "name": "Jamnik",
            "email": "jan@pawel.ii"
        }
}
```

### Quizes
### Get list of `quiz` objects

```
GET /api/admin/quiz
```
Sample Request:
```
{}
```
Sample Response:
```
{
    "quizzes": [
        {
            "id": 1,
            "name": "Test quiz",
            "access_token": "6KVo2Qn7MJt2vmJ",
            "registration_fields": [
                {
                    "type": "text",
                    "name": "gender",
                    "required": true
                },
                {
                    "type": "email",
                    "name": "email",
                    "required": true
                },
                {
                    "type": "number",
                    "name": "age",
                    "required": false
                }
            ],
            "started": false,
            "finished": false,
            "creation_date": "2019-10-28T16:24:49",
            "questions": [
                {
                    "id": 1,
                    "question": "What will You choose?",
                    "answers": null
                },
                {
                    "id": 2,
                    "question": "What will You choose again?",
                    "answers": null
                }
            ]
        }
    ]
}
```
### Creates new quiz

```
POST /api/admin/quiz
```
Sample Request:
```
{
    "name": "Test quiz",
    "registration_fields": [
        {
            "type": "text",
            "name": "gender",
            "required": true
        },
        {
            "type": "email",
            "name": "email",
            "required": true
        },
        {
            "type": "number",
            "name": "age",
            "required": false
        }
    ],
    "questions": [
        {
            "question": "What will You choose?",
            "answers": [
                {
                    "value": "Option 1",
                    "is_correct": true
                },
                {
                    "value": "Option 2",
                    "is_correct": false
                }
            ]
        },
        {
            "question": "What will You choose again?",
            "answers": [
                {
                    "value": "Option 21",
                    "is_correct": false
                },
                {
                    "value": "Option 22",
                    "is_correct": true
                }
            ]
        }
    ]
}
```
Sample Response:
```
{
    "id": 1,
    "name": "Test quiz",
    "access_token": "6KVo2Qn7MJt2vmJ",
    "registration_fields": [
        {
            "type": "text",
            "name": "gender",
            "required": true
        },
        {
            "type": "email",
            "name": "email",
            "required": true
        },
        {
            "type": "number",
            "name": "age",
            "required": false
        }
    ],
    "started": false,
    "finished": false,
    "creation_date": "2019-10-28T16:24:49",
    "questions": [
        {
            "id": 1,
            "question": "What will You choose?",
            "answers": null
        },
        {
            "id": 2,
            "question": "What will You choose again?",
            "answers": null
        }
    ]
}
```
### Gets single `quiz` object

```
GET /api/admin/quiz/<quiz_id>
```
Sample Request:
```
{}
```
Sample Response:
```
{
    "id": 2,
    "name": "Test quiz",
    "access_token": "rXv9LNzPX3I24ny",
    "registration_fields": [
        {
            "type": "text",
            "name": "gender",
            "required": true
        },
        {
            "type": "email",
            "name": "email",
            "required": true
        },
        {
            "type": "number",
            "name": "age",
            "required": false
        }
    ],
    "started": false,
    "finished": false,
    "creation_date": "2019-10-28T16:35:50",
    "questions": [
        {
            "id": 3,
            "question": "What will You choose?",
            "answers": null
        },
        {
            "id": 4,
            "question": "What will You choose again?",
            "answers": null
        }
    ]
}
```
### Updates quiz: excluding values like: `creation_date` or `id`

```
PUT /api/admin/quiz/<quiz_id>
```
Sample Request: 
```
{
    "name": "Test quiz poprawka",
    "registration_fields": [
        {
            "type": "text",
            "name": "gender",
            "required": true
        },
        {
            "type": "number",
            "name": "age",
            "required": false
        }
    ],
    "questions": [
        {
            "question": "What will You choose?",
            "answers": [
                {
                    "value": "Option 1",
                    "is_correct": true
                },
                {
                    "value": "Option 2",
                    "is_correct": false
                }
            ]
        },
        {
            "question": "What will You choose again xd?",
            "answers": [
                {
                    "value": "Option 21",
                    "is_correct": false
                },
                {
                    "value": "Option 22",
                    "is_correct": true
                },
                {
                    "value": "Option 23",
                    "is_correct": false
                }
            ]
        },
        {
            "question": "Are you kabala?",
            "answers": [
                {
                    "value": "No",
                    "is_correct": false
                },
                {
                    "value": "Yes",
                    "is_correct": true
                }
            ]
        }
    ]
}
```
Sample Response:
```
{
    "id": 2,
    "name": "Test quiz poprawka",
    "access_token": "2XVAR16ygeFkx8M",
    "registration_fields": [
        {
            "type": "text",
            "name": "gender",
            "required": true
        },
        {
            "type": "number",
            "name": "age",
            "required": false
        }
    ],
    "started": false,
    "finished": false,
    "creation_date": "2019-10-28T20:05:35",
    "questions": [
        {
            "id": 94,
            "question": "What will You choose?",
            "answers": [
                {
                    "id": 130,
                    "value": "Option 1",
                    "is_correct": true
                },
                {
                    "id": 131,
                    "value": "Option 2",
                    "is_correct": false
                }
            ]
        },
        {
            "id": 95,
            "question": "What will You choose again xd?",
            "answers": [
                {
                    "id": 132,
                    "value": "Option 21",
                    "is_correct": false
                },
                {
                    "id": 133,
                    "value": "Option 22",
                    "is_correct": true
                },
                {
                    "id": 134,
                    "value": "Option 23",
                    "is_correct": false
                }
            ]
        },
        {
            "id": 96,
            "question": "Are you kabala?",
            "answers": [
                {
                    "id": 135,
                    "value": "No",
                    "is_correct": false
                },
                {
                    "id": 136,
                    "value": "Yes",
                    "is_correct": true
                }
            ]
        }
    ]
}
```
### Deletes the quiz

```
DELETE /api/admin/quiz/<quiz_id> 
```
Sample Request:
```
{}
```
Sample Response:
```
{
    "success": true
}
```
### Starts the quiz

```
PUT /api/admin/quiz/<quiz_id>/start
```
Sample Request:
```
{}
```
Sample Response:
```
{
    "success": true
}
```
Finishes the quiz

```
PUT /api/admin/quiz/<quiz_id>/finish
```
Sample Request:
```
{}
```
Sample Response:
```
{
    "success": true
}
```

### Reports
```
GET /api/admin/quiz/<quiz_id>/report
```
Get the report of quiz, answers with info how many times was answered and users, example:
```
{
    "id": 1,
    "name": "Test quiz",
    "started": true,
    "finished": false,
    "creation_date": "2006-01-02T15:04:05",
    "finish_date": null,
    "users": [
        {
            "id": "sdasgyd3r64yrvf"
            "name": "Adrian"
        },
        ...
    ],
    "questions": [
        {
            "id": 1,
            "question": "What will You choose?"
            "answers": [
                {
                    "id": 1,
                    "value": "Option 1",
                    "is_correct": true,
                    "answers": 3
                },
                {
                    "id": 2,
                    "value": "Option 2",
                    "is_correct": false,
                    "answers": 2
                },
                ...
            ]
        },
        {
            "id": 2,
            "question": "What will You choose again?"
            "answers": [
                {
                    "id": 1,
                    "value": "Option 21",
                    "is_correct": false,
                    "answers": 1
                },
                {
                    "id": 2,
                    "value": "Option 22",
                    "is_correct": true,
                    "answers": 4
                },
                ...
            ]
        },
        ...
    ]
}
```

```
GET /api/admin/quiz/<quiz_id>/report/<user_id>
```
Get reports and info about specified user, example:
```
{
    "user": {
        "id": "sdasgyd3r64yrvf"
        "name": "Adrian",
        fields: [
            {
                "type": "text",
                "name": "gender",
                "value": "mucha"
            },
            {
                "type": "email",
                "name": "email",
                "value": "a.slucha@gmail.com"
            },
            {
                "type": "number",
                "name": "age",
                "value": 69
            }
        ],
        "geolocation": {
                "city": "",
                "country": "",
                "ip": "172.18.0.1"
            }
    },
    "answers" [
        {
            "id": 1,
            "question": "What will You choose?"
            "answer": {
                "id": 1,
                "value": "Option 1",
                "is_correct": true,
            }
        },
        {
            "id": 2,
            "question": "What will You choose again?"
            "answer": {
                "id": 2,
                "value": "Option 22",
                "is_correct": true,
            }
        },
        .
        ..
    ]
}
```

### User endpoints

### Get `quiz` object for given user
```
GET /api/user/quiz
```
Sample Request:
```
{}
```
Sample Response:
```
{
    "id": 5,
    "name": "Test quiz poprawka",
    "access_token": "92vEjD656qf2Vzr",
    "registration_fields": [
        {
            "type": "text",
            "name": "gender",
            "required": true
        },
        {
            "type": "number",
            "name": "age",
            "required": false
        }
    ],
    "started": true,
    "finished": false,
    "creation_date": "2019-10-28T20:25:02",
    "questions": [
        {
            "id": 107,
            "question": "What will You choose?",
            "answers": [
                {
                    "id": 159,
                    "value": "Option 1",
                    "is_correct": true
                },
                {
                    "id": 160,
                    "value": "Option 2",
                    "is_correct": false
                }
            ]
        },
        {
            "id": 108,
            "question": "What will You choose again xd?",
            "answers": [
                {
                    "id": 161,
                    "value": "Option 21",
                    "is_correct": false
                },
                {
                    "id": 162,
                    "value": "Option 22",
                    "is_correct": true
                },
                {
                    "id": 163,
                    "value": "Option 23",
                    "is_correct": false
                }
            ]
        },
        {
            "id": 109,
            "question": "Are you kabala?",
            "answers": [
                {
                    "id": 164,
                    "value": "No",
                    "is_correct": false
                },
                {
                    "id": 165,
                    "value": "Yes",
                    "is_correct": true
                }
            ]
        }
    ]
}
```

### Get results of quiz for given user if available
```
GET /api/user/results
```
Sample Request:
```
{}
```
Sample Response:
```
{
    "id": 5,
    "name": "Test quiz poprawka",
    "creation_date": "2019-10-28T20:25:02",
    "answers": [
        {
            "question": "What will You choose?",
            "answer": "-",
            "correct_answer": "Option 1",
            "is_correct": false
        },
        {
            "question": "What will You choose again xd?",
            "answer": "Option 22",
            "is_correct": true
        },
        {
            "question": "Are you kabala?",
            "answer": "No",
            "correct_answer": "Yes",
            "is_correct": false
        }
    ]
}
```

### Updates answer for given question for user
```
PUT /api/user/quiz/<quiz_id>/answer
```
Sample Request:
```
{
    "question_id": 108,
    "answer_id": 163
}
```
Sample Response:
```
{
    "answers": [
        {
            "question_id": 108,
            "answer_id": 163
        },
        {
            "question_id": 109,
            "answer_id": 164
        }
    ]
}
```
Multi question:
Sample Request:
```
{
    "question_id": 108,
    "answer_id": 163
}
```
Sample Response:
```
{
    "answers": [
        {
            "question_id": 40,
            "answer_id": 0,
            "time_taken": 0,
            "answer_ids": [
                46,
                47
            ]
        }
    ]
}
```

### Marks quiz as finished by user
```
PUT /api/user/quiz/<quiz_id>/finish
```
Sample Request:
```
{}
```
Sample Response:
```
{
    "questions": 10,
    "correct_answers": 4,
    "missing_answers": 2,
    "over_time": 1
}
```

### Events

### Sending events as admin
```
POST /api/admin/quiz/<quiz_id>/validate
```
Sample Request:
```
{
	"values": ["jan", "pawel", "2"],
	"correct": "jan"
}
```
Sample Response:
```
{
    "success": true
}
```

### Answering events as user
```
PUT /api/user/quiz/<quiz_id>/validate
```
Sample Request:
```
{
	"answer": "jan",
	"event_id": 79
}
```
Sample Response:
```
{
    "success": true
}
```

### WebSocket
You can connect to websocket on:
```
WS: ws://<app_url>/ws
``` 
On websockets there are broadcasted validation events in form like:
```
{
    "quiz_id":36,
    "event_id":79,
    "payload":{
        "values":["jan","pawel","2"]
    }
}
```

### Errors
Errors comes in format:
```
{
    "error": "Reason why it occurred"
}
```
