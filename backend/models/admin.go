package models

import (
	"errors"
	"kizzy/api/objects"
	"kizzy/api/utils"
	"kizzy/config"
	"kizzy/db"
	"log"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

type Admin struct {
	gorm.Model
	Name     string
	Email    string
	Password string
	Quizes   []*Quiz
	Token    string `sql:"-"`
}

func (a *Admin) validate(object *objects.AdminObject) error {
	temp := &Admin{}
	err := db.GetDB().Set("gorm:auto_preload", true).Table("admins").Where("email = ?", object.Email).First(temp).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return err
	}
	if temp.Email == object.Email {
		return utils.NewApiError(http.StatusConflict, "Admin with such a mail already exists")
	}
	return nil
}

func CreateAdmin(object *objects.AdminObject) (*Admin, error) {
	admin := &Admin{}
	if err := admin.validate(object); err != nil {
		return nil, err
	}
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(object.Password), bcrypt.DefaultCost)

	admin.Email = object.Email
	admin.Name = object.Name
	admin.Password = string(hashedPassword)

	db.GetDB().Create(admin)

	if admin.ID <= 0 {
		return nil, errors.New("Failed to create account, connection error.")
	}

	tk := &Token{UserId: admin.ID, UserType: "admin"}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	tokenString, _ := token.SignedString([]byte(config.RetreiveConfig().Auth.JWTPassword))
	admin.Token = tokenString

	return admin, nil
}

func GetAdmin(id uint) (*Admin, error) {
	a := &Admin{}
	err := db.GetDB().Set("gorm:auto_preload", true).Table("admins").Where("id = ?", id).First(a).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, utils.NewApiError(http.StatusNotFound, "Admin do not exists")
		}
		return nil, err
	}
	return a, nil
}

func LoginAdmin(login, password string) (*Admin, error) {

	admin := &Admin{}
	err := db.GetDB().Table("admins").Where("email = ?", login).First(admin).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			log.Println("Not found")
			return nil, utils.NewApiError(http.StatusUnauthorized, "Wrong email or password")
		}
		return nil, errors.New("Failed to retreive account, connection error.")
	}

	err = bcrypt.CompareHashAndPassword([]byte(admin.Password), []byte(password))
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		log.Println("Wrong password")
		return nil, utils.NewApiError(http.StatusUnauthorized, "Wrong email or password")
	}

	tk := &Token{UserId: admin.ID, UserType: "admin"}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	tokenString, _ := token.SignedString([]byte(config.RetreiveConfig().Auth.JWTPassword))
	admin.Token = tokenString

	return admin, nil
}
