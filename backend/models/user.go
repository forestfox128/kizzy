package models

import (
	"errors"
	"kizzy/api/objects"
	"kizzy/api/utils"
	"kizzy/config"
	"kizzy/db"
	"log"
	"net"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"github.com/lib/pq"
	"github.com/oschwald/geoip2-golang"
)

func uintToIntArray(in []uint) []int64 {
	data := make([]int64, len(in))
	for i, id := range in {
		data[i] = int64(id)
	}
	return data
}

func intToUIntArray(in []int64) []uint {
	data := make([]uint, len(in))
	for i, id := range in {
		data[i] = uint(id)
	}
	return data
}

type RegisteredFormField struct {
	gorm.Model
	Type        string
	Name        string
	ValueString string
	ValueInt    int
	UserID      uint
}

type UserAnswer struct {
	gorm.Model
	QuestionID uint
	AnswerID   uint
	AnswerIDs  pq.Int64Array `gorm:"type:integer[]"`
	UserID     uint
	TimeTaken  uint
}

type ValidationAnswer struct {
	gorm.Model
	EventID    uint
	IsCorrect  bool
	UserAnswer string
	UserID     uint
}

type User struct {
	gorm.Model
	Name              string
	UserFields        []*RegisteredFormField
	UserAnswers       []*UserAnswer
	ValidationAnswers []*ValidationAnswer
	QuizID            uint
	Active            bool
	HasFinishedQuiz   bool
	GeoLoc            objects.UserGeoLocObject `gorm:"embedded"`
	Token             string                   `sql:"-"`
}

func CreateUser(quizToken string) (*User, error) {
	q := &Quiz{}
	err := db.GetDB().Table("quizzes").Where("access_token = ?", quizToken).First(q).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, utils.NewApiError(http.StatusNotFound, "Wrong access_token provided")
		}
		return nil, err
	}
	if q.Finished {
		return nil, utils.NewApiError(http.StatusBadRequest, "Quiz is already finished")
	}
	user := &User{QuizID: q.ID}
	db.GetDB().Create(user)
	if user.ID <= 0 {
		return nil, errors.New("Failed to create user, connection error.")
	}

	tk := &Token{UserId: user.ID, UserType: "user"}
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	tokenString, _ := token.SignedString([]byte(config.RetreiveConfig().Auth.JWTPassword))
	user.Token = tokenString

	return user, nil
}

func GetUsersByQuizID(id uint) ([]*User, error) {
	users := make([]*User, 0)
	if err := db.GetDB().Set("gorm:auto_preload", true).Table("users").Where("quiz_id = ?", id).Find(&users).Error; err != nil {
		return nil, err
	}
	return users, nil
}

func GetUser(id uint) (*User, error) {
	u := &User{}

	err := db.GetDB().Set("gorm:auto_preload", true).Table("users").Where("id = ?", id).First(u).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, utils.NewApiError(http.StatusNotFound, "User do not exists")
		}
		return nil, err
	}
	return u, nil
}

func (u *User) ActivateUser(name string, formFields []*RegisteredFormField, geoLoc *geoip2.City, ip net.IP) error {
	u.Active = true
	u.Name = name
	u.UserFields = formFields

	if geoLoc != nil {
		log.Println(geoLoc, ip)
		u.GeoLoc = objects.UserGeoLocObject{
			City:    geoLoc.City.Names["en"],
			Country: geoLoc.Country.Names["en"],
			IP:      ip.String(),
		}
	}

	if err := db.GetDB().Table("users").Save(u).Error; err != nil {
		return err
	}

	return nil
}

func (u *User) UpdateAnswer(newAnswer objects.UserAnswerObject, isMulti bool) (objects.UserAnswersObject, error) {
	var userAnswer *UserAnswer

	for _, answer := range u.UserAnswers {
		if answer.QuestionID == newAnswer.QuestionID {
			userAnswer = answer
			break
		}
	}

	if userAnswer != nil {
		if isMulti {
			userAnswer.AnswerIDs = uintToIntArray(newAnswer.AnswerIDs)
		} else {
			userAnswer.AnswerID = newAnswer.AnswerID
		}
	} else {
		userAnswer = &UserAnswer{
			UserID:     u.ID,
			QuestionID: newAnswer.QuestionID,
			TimeTaken:  newAnswer.TimeTaken,
		}
		if isMulti {
			userAnswer.AnswerIDs = uintToIntArray(newAnswer.AnswerIDs)
		} else {
			userAnswer.AnswerID = newAnswer.AnswerID
		}
		u.UserAnswers = append(u.UserAnswers, userAnswer)
	}

	if err := u.SaveUser(); err != nil {
		return nil, err
	}

	u, err := GetUser(u.ID)
	if err != nil {
		return nil, err
	}

	answersObject := make(objects.UserAnswersObject, 0, len(u.UserAnswers))
	for _, answer := range u.UserAnswers {
		answersObject = append(answersObject, &objects.UserAnswerObject{
			AnswerID:   answer.AnswerID,
			AnswerIDs:  intToUIntArray(answer.AnswerIDs),
			QuestionID: answer.QuestionID,
		})
	}
	log.Println(answersObject)
	return answersObject, nil
}

func (u *User) AddValidationAnswer(vaID uint, answer string, isCorrect bool) error {
	var vaDB *ValidationAnswer

	for _, validation := range u.ValidationAnswers {
		if validation.EventID == vaID {
			vaDB = validation
			break
		}
	}

	if vaDB != nil {
		return utils.NewApiError(http.StatusBadRequest, "User already answered to event")
	} else {
		vaDB = &ValidationAnswer{
			EventID:    vaID,
			UserAnswer: answer,
			IsCorrect:  isCorrect,
		}
		u.ValidationAnswers = append(u.ValidationAnswers, vaDB)
	}

	return u.SaveUser()
}

func (u *User) SaveUser() error {
	if err := db.GetDB().Table("users").Save(u).Error; err != nil {
		return err
	}

	return nil
}

func (u *User) MarkQuizAsFinished() error {
	u.HasFinishedQuiz = true

	if err := db.GetDB().Table("users").Save(u).Error; err != nil {
		return err
	}

	return nil
}

func (u *User) GenerateUserObject() *objects.UserObject {
	userObject := &objects.UserObject{
		ID:       u.ID,
		Username: u.Name,
	}

	userFields := make(objects.RegistrationFields, 0, len(u.UserFields))
	for _, field := range u.UserFields {
		userField := &objects.RegistrationField{
			Name: field.Name,
		}

		switch field.Type {
		case "text", "email", "dropdown", "select":
			userField.Value = field.ValueString
		case "number":
			userField.Value = field.ValueInt
		}
		userFields = append(userFields, userField)
	}
	userObject.UserFields = userFields
	userObject.GeoLoc = u.GeoLoc
	log.Println(u.GeoLoc)

	userEvents := make(objects.UserEventAnswers, 0, len(u.ValidationAnswers))
	for _, event := range u.ValidationAnswers {
		userEvents = append(userEvents, &objects.UserEventAnswer{
			Answer:    event.UserAnswer,
			IsCorrect: event.IsCorrect,
		})
	}
	userObject.UserEventsAnswers = userEvents

	return userObject
}

func (ua *UserAnswer) GetAnswersIDs() []uint {
	return intToUIntArray(ua.AnswerIDs)
}
