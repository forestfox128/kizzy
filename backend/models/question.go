package models

import "github.com/jinzhu/gorm"

type Answer struct {
	gorm.Model
	Value      string
	IsCorrect  bool
	QuestionID uint
}

type Question struct {
	gorm.Model
	IsMulti      bool
	QuestionTime uint
	Value        string
	Answers      []*Answer
	QuizID       uint
}
