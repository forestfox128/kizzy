package models

import "github.com/jinzhu/gorm"

func MigrateModels(db *gorm.DB) {
	db.Debug().AutoMigrate(
		&Admin{},
		&User{},
		&UserAnswer{},
		&RegisteredFormField{},
		&Answer{},
		&Question{},
		&RegistrationFormField{},
		&Quiz{},
		&ValidationEvent{},
		&ValidationAnswer{})
}
