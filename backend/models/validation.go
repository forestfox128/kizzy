package models

import (
	"github.com/jinzhu/gorm"
	"github.com/lib/pq"
)

type ValidationEvent struct {
	gorm.Model
	QuizID       uint
	Values       pq.StringArray `gorm:"type:varchar(128)[]"`
	CorrectValue string
}
