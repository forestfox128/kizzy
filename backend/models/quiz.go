package models

import (
	"errors"
	"kizzy/api/objects"
	"kizzy/api/utils"
	"kizzy/config"
	"kizzy/db"
	"net/http"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/lib/pq"
	"github.com/speps/go-hashids"
)

type RegistrationFormField struct {
	gorm.Model
	Type     string
	Name     string
	Required bool
	QuizID   uint
	Values   pq.StringArray `gorm:"type:varchar(128)[]"`
	IsSet    bool           `sql:"-"`
}

func (rf *RegistrationFormField) ValidateValue(inValue string) bool {
	for _, value := range rf.Values {
		if inValue == value {
			return true
		}
	}
	return false
}

type Quiz struct {
	gorm.Model
	Name                   string
	AccessToken            string
	RegistrationFormFields []*RegistrationFormField
	Started                bool
	Finished               bool
	FinishedTimestamp      int64
	Questions              []*Question
	AdminID                uint
	ValidationEvents       []*ValidationEvent
	Randomize              bool
}

func CreateQuiz(quiz *objects.QuizObject, registrationField []*RegistrationFormField, questions []*Question, adminID uint) (*Quiz, error) {
	hd := hashids.NewData()
	hd.MinLength = 15
	hd.Salt = config.RetreiveConfig().IDSalt
	h, err := hashids.NewWithData(hd)
	if err != nil {
		return nil, err
	}
	accessToken, err := h.Encode([]int{int(time.Now().Unix()), int(adminID)})
	if err != nil {
		return nil, err
	}

	q := &Quiz{
		Name:                   quiz.Name,
		AccessToken:            accessToken,
		RegistrationFormFields: registrationField,
		Questions:              questions,
		AdminID:                adminID,
		Randomize:              quiz.Randomize,
	}
	db.GetDB().Create(q)
	if q.ID <= 0 {
		return nil, errors.New("Failed to create user, connection error.")
	}

	return q, nil
}

func GetQuiz(id uint) (*Quiz, error) {
	q := &Quiz{}
	err := db.GetDB().Set("gorm:auto_preload", true).Table("quizzes").Where("id = ?", id).First(q).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, utils.NewApiError(http.StatusNotFound, "Quiz do not exists")
		}
		return nil, err
	}
	return q, nil
}

func GetQuizzesForAdmin(adminID uint) ([]Quiz, error) {
	q := []Quiz{}
	err := db.GetDB().Set("gorm:auto_preload", true).Table("quizzes").Where("admin_id = ?", adminID).Find(&q).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, utils.NewApiError(http.StatusNotFound, "Quiz do not exists")
		}
		return nil, err
	}
	return q, nil
}

func (q *Quiz) Start() error {
	if q.Started {
		return utils.NewApiError(http.StatusBadRequest, "Quiz is already activated")
	}
	if q.Finished {
		return utils.NewApiError(http.StatusBadRequest, "Quiz is already finished")
	}
	q.Started = true
	if err := db.GetDB().Table("quizzes").Save(q).Error; err != nil {
		return err
	}
	return nil
}

func (q *Quiz) Finish() error {
	if !q.Started {
		return utils.NewApiError(http.StatusBadRequest, "You can't finish quiz that haven't started")
	}
	if q.Finished {
		return utils.NewApiError(http.StatusBadRequest, "Quiz is already finished")
	}
	q.Finished = true
	if err := db.GetDB().Table("quizzes").Save(q).Error; err != nil {
		return err
	}
	return nil
}

func (q *Quiz) AddEvent(event objects.ValidationEvent) (*ValidationEvent, error) {
	if !q.Started {
		return nil, utils.NewApiError(http.StatusBadRequest, "You can't finish quiz that haven't started")
	}
	if q.Finished {
		return nil, utils.NewApiError(http.StatusBadRequest, "Quiz is already finished")
	}

	ve := &ValidationEvent{
		Values:       event.EventValues,
		CorrectValue: event.CorrectValue,
	}

	if q.ValidationEvents == nil {
		q.ValidationEvents = append(q.ValidationEvents, ve)
	} else {
		q.ValidationEvents = make([]*ValidationEvent, 0)
		q.ValidationEvents = append(q.ValidationEvents, ve)
	}

	if err := q.Update(); err != nil {
		return nil, err
	}
	return ve, nil
}

func (q *Quiz) ValidateEvent(eventID uint, answer string) (bool, error) {
	for _, event := range q.ValidationEvents {
		if event.ID == eventID {
			found := false
			for _, eventAnswer := range event.Values {
				if eventAnswer == answer {
					found = true
					break
				}
			}
			if !found {
				return false, utils.NewApiError(http.StatusBadRequest, "There is no such answer for given `event_id`")
			}
			if event.CorrectValue != answer {
				return false, nil
			}
			return true, nil
		}
	}
	return false, utils.NewApiError(http.StatusBadRequest, "There is no such a event for given `quiz_id`")
}

func (q *Quiz) DeleteQuestions() error {
	for _, question := range q.Questions {
		if err := db.GetDB().Delete(question).Error; err != nil {
			return err
		}
	}
	return nil
}

func (q *Quiz) DeleteFormFields() error {
	for _, field := range q.RegistrationFormFields {
		if err := db.GetDB().Delete(field).Error; err != nil {
			return err
		}
	}
	return nil
}

func (q *Quiz) Delete() error {
	err := db.GetDB().Delete(q).Error
	if err != nil {
		return err
	}
	return nil
}

func (q *Quiz) Update() error {
	if err := db.GetDB().Table("quizzes").Save(q).Error; err != nil {
		return err
	}
	return nil
}

func (quiz *Quiz) GetObjectFromModel(forUser bool) *objects.QuizObject {
	startDate := quiz.CreatedAt.Format("2006-01-02T15:04:05")
	quizObject := &objects.QuizObject{
		ID:           quiz.ID,
		Name:         quiz.Name,
		AccessToken:  quiz.AccessToken,
		CreationDate: startDate,
		Started:      quiz.Started,
		Finished:     quiz.Finished,
	}
	if quiz.FinishedTimestamp != 0 {
		finishDate := time.Unix(quiz.FinishedTimestamp, 0).Format("2006-01-02T15:04:05")
		quizObject.FinishDate = finishDate
	}
	questions := make(objects.QuestionsObject, 0, len(quiz.Questions))
	for _, question := range quiz.Questions {
		answers := make(objects.AnswersObject, 0, len(question.Answers))
		for _, answer := range question.Answers {
			answers = append(answers, &objects.AnswerObject{
				ID:        answer.ID,
				Value:     answer.Value,
				IsCorrect: answer.IsCorrect,
			})
		}
		questions = append(questions, &objects.QuestionObject{
			ID:           question.ID,
			Question:     question.Value,
			QuestionTime: question.QuestionTime,
			IsMulti:      question.IsMulti,
			Answers:      answers,
		})
	}

	formFields := make(objects.RegistrationFields, 0, len(quiz.RegistrationFormFields))
	for _, formField := range quiz.RegistrationFormFields {
		var fields []string
		if !(forUser && formField.Type == "select") {
			fields = formField.Values
		}
		formFields = append(formFields, &objects.RegistrationField{
			FieldType:      formField.Type,
			Name:           formField.Name,
			Required:       formField.Required,
			PossibleValues: fields,
		})
	}

	quizObject.Questions = questions
	quizObject.RegistrationFields = formFields
	quizObject.Randomize = quiz.Randomize
	return quizObject
}
