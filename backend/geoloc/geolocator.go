package geolocate

import (
	"log"
	"net"

	"github.com/oschwald/geoip2-golang"
)

var geoDB *geoip2.Reader

func Init(fileName string) {
	db, err := geoip2.Open(fileName)
	if err != nil {
		log.Fatal(err)
	}
	geoDB = db
}

func GetGeoForIP(ip net.IP) *geoip2.City {
	city, err := geoDB.City(ip)
	if err != nil {
		log.Println(err)
		return nil
	}

	return city
}
