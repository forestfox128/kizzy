import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { ReportsModule } from './charts';
import { AuthModule } from './auth';
import { HomeModule } from './home';
import { QuizModule } from './quiz';

@NgModule({
  declarations: [AppComponent],
  imports: [
    AppRoutingModule,
    BrowserModule,
    ReportsModule,
    AuthModule,
    HomeModule,
    QuizModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
