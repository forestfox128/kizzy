import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { QuizApiService } from 'src/app/api';
import { combineLatest, of } from 'rxjs';
import { isNil, pathOr, last, head } from 'ramda';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent implements OnInit {
  userReport$ = of(null);
  selectedUser = null;
  report: any = {};
  quiz: any = {};
  id: string;

  constructor(
    private service: QuizApiService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  barChartData = [{ data: [], label: '' }];
  barChartOptions = {
    maintainAspectRatio: false,
    responsive: true
  };
  barChartLegend = true;
  barChartType = 'bar';
  barChartLabels = [];
  chartColors = [
    {
      backgroundColor: 'rgba(77,83,96,0.2)'
    },
    {
      backgroundColor: 'rgba(0,0,0,0.5)'
    }
  ];

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getData();
  }

  getData() {
    combineLatest([
      this.service.getQuiz(this.id),
      this.service.getQuizDetails(this.id)
    ]).subscribe(([quiz, report]) => {
      this.quiz = quiz;
      this.report = report;
      this.fillData();
    });
  }

  fillData() {
    this.barChartLabels = this.report.questions.map(ques => ques.question);
    this.barChartData = [
      {
        data: this.report.questions.map(ques =>
          pathOr(0, ['anwsered'])(head(ques.answers))
        ),
        label: 'Correct Answers'
      },
      {
        data: this.report.questions.map(ques =>
          pathOr(0, ['anwsered'])(last(ques.answers))
        ),
        label: 'Incorrect Answers'
      }
    ];
  }

  startQuiz() {
    this.service.startQuiz(this.id).subscribe(_ => this.getData());
  }

  finishQuiz() {
    this.service.finishQuiz(this.id).subscribe(_ => this.getData());
  }

  deleteQuiz() {
    this.service
      .deleteQuiz(this.id)
      .subscribe(_ => this.router.navigate(['quizes']));
  }

  selectUser(id: string) {
    if (!isNil(id) && typeof id === 'string') {
      this.userReport$ = this.service.getQuizForUser(this.id, id);
      this.selectedUser = id;
    } else {
      this.selectedUser = null;
    }
  }
}
