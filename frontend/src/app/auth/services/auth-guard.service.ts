import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthApiService } from 'src/app/api';
import { Observable, merge, combineLatest, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private service: AuthApiService, private router: Router) {}

  canActivate(): Observable<boolean> {
    return combineLatest([
      this.service.isTokenValid(),
      this.service.isLoggedIn
    ]).pipe(
      map((fromApi, fromApp) => {
        if (fromApi || fromApp) {
          return true;
        }
        this.router.navigate(['/login']);
        return false;
      }),
      catchError(_ => {
        this.router.navigate(['/login']);
        return of(false);
      })
    );
  }
}
